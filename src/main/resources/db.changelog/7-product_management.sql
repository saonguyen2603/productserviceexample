CREATE TABLE product_management
(
    id                                  bigserial primary key,
    code                                varchar,
    name                                varchar,
    product_catalog_management_id       BIGINT references product_catalog_management (id),
    producer                            varchar,
    unit                                varchar,
    description                         varchar,
    uses                                varchar,
    batch_code_generation_support       boolean,
    information_properties              boolean,
    information_properties_how_to_enter varchar,
    price_variation                     boolean,
    price_variation_how_to_enter        varchar,
    is_status                           boolean,
    created_at                          DATE,
    updated_at                          DATE,
    created_by                          BIGINT,
    updated_by                          BIGINT
);

CREATE TABLE product_management_picture
(
    id                        bigserial primary key,
    product_management_id     BIGINT references product_management (id),
    product_picture_link      varchar,
    public_id_product_picture varchar
);

CREATE TABLE product_management_map_property
(
    id                            bigserial primary key,
    product_management_id         BIGINT references product_management (id),
    attribute_group_management_id BIGINT references attribute_group_management (id),
    property_management_id        BIGINT references property_management (id),
    value                         varchar
);

CREATE TABLE product_management_map_variation
(
    id                            bigserial primary key,
    product_management_id         BIGINT references product_management (id),
    price_variation_management_id BIGINT references price_variation_management (id),
    code_product                  varchar
);

CREATE TABLE product_management_map_picture_variation
(
    id                          bigserial primary key,
    product_management_map_variation_id   BIGINT references product_management_map_variation (id),
    link_picture_variation      varchar,
    public_id_picture_variation varchar
);