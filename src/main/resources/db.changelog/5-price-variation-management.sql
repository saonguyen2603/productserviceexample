CREATE table "price_variation_management"
(
    id         bigserial primary key,
    name       varchar,
    code       varchar,
    color_type varchar,
    created_at DATE,
    updated_at DATE,
    created_by BIGINT,
    updated_by BIGINT
);

CREATE TABLE "price_variation_management_map"
(
    id                            bigserial primary key,
    price_variation_management_id bigint references price_variation_management (id),
    name_value                    varchar,
    order_number                  bigint,
    is_default                    boolean,
    created_at                    DATE,
    updated_at                    DATE,
    created_by                    BIGINT,
    updated_by                    BIGINT
)