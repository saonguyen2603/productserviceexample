ALTER TABLE product_management_map_variation add column created_at DATE;
ALTER TABLE product_management_map_variation add column updated_at DATE;
ALTER TABLE product_management_map_variation add column created_by BIGINT;
ALTER TABLE product_management_map_variation add column updated_by BIGINT;
