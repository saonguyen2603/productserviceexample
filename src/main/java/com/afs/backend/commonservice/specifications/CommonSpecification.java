package com.afs.backend.commonservice.specifications;

import com.afs.backend.commonservice.utils.Constants;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class CommonSpecification<T> {
    public Specification<T> withIsActive(String value) {
        Boolean boolValue = "true".equalsIgnoreCase(value) ? Boolean.TRUE :
                "false".equalsIgnoreCase(value) ? Boolean.FALSE : null;
        if (boolValue == null) {
            return null;
        }

        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get("isActive"), boolValue);
    }

    public Specification<T> withGreaterThanOrEqual(String column, String value) {
        if (value.isEmpty()) {
            return null;
        }
        return (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.greaterThanOrEqualTo(root.get(column), value);

    }

    public Specification<T> withLessThanOrEqual(String column, String value) {
        if (value.isEmpty()) {
            return null;
        }
        return (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.lessThanOrEqualTo(root.get(column), value);
    }

    public Specification<T> withGreater(String column, String value) {
        return (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.greaterThan(root.get(column), value);
    }

    public Specification<T> withLess(String column, String value) {
        return (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.lessThan(root.get(column), value);
    }


    public Specification<T> withFilter(Map<String, String> filterColumns, List<String> searchColumns,
                                       Map<String, String> filterParam, String lang) {

        return ((root, criteriaQuery, criteriaBuilder) -> {

            List<Predicate> predicates = new ArrayList<>();
            filterParam.forEach((key, value) -> {
                if (value != null && value.length() > 0) {
                    if (key.equalsIgnoreCase(Constants.SEARCH_KEY)) {
                        // add search
                        searchColumns.forEach(columnSearch -> {
                            predicates.add(criteriaBuilder.like(root.get(columnSearch), "%" + value + "%"));
                        });
                        criteriaBuilder.or(predicates.toArray(new Predicate[0]));
                    }

                    if (!Objects.isNull(filterColumns)) {
                        String column = filterColumns.get(key);
                        if (column != null) {
                            // Filter in column
                            boolean isBoolean = Boolean.parseBoolean(value);
                            if (isBoolean) {
                                Boolean value1 = "true".equalsIgnoreCase(value) ? Boolean.TRUE :
                                        "false".equalsIgnoreCase(value) ? Boolean.FALSE : null;
                                predicates.add(criteriaBuilder.equal(root.get(column), value1));
                            } else {
                                predicates.add(criteriaBuilder.equal(root.get(column), value));
                            }
                        }
                    }

                }
            });

            if (predicates.size() <= 0) {
                return null;
            }
            return criteriaBuilder.or(predicates.toArray(new Predicate[0]));
        });
    }
}
