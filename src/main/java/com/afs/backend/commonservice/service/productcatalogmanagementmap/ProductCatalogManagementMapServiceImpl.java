package com.afs.backend.commonservice.service.productcatalogmanagementmap;

import com.afs.backend.commonservice.dto.productcatalogmanagementmap.ProductCatalogManagementMapPriceVariationOutput;
import com.afs.backend.commonservice.entity.ProductCatalogManagementMapEntity;
import com.afs.backend.commonservice.repository.PriceVariationManagementRepository;
import com.afs.backend.commonservice.repository.ProductCatalogManagementMapRepository;
import com.afs.backend.commonservice.service.pricevariationmanagement.PriceVariationManagementService;
import com.afs.backend.commonservice.service.propertymanagement.PropertyManagementService;
import com.afs.backend.commonservice.dto.productcatalogmanagementmap.ProductCatalogManagementMapAttributeOutput;
import com.afs.backend.commonservice.repository.AttributeGroupManagementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductCatalogManagementMapServiceImpl implements ProductCatalogManagementMapService{
    @Autowired
    private ProductCatalogManagementMapRepository repository;
    @Autowired
    private PropertyManagementService propertyManagementService;
    @Autowired
    private AttributeGroupManagementRepository attributeGroupManagementRepository;
    @Autowired
    private PriceVariationManagementRepository priceVariationManagementRepository;
    @Autowired
    private PriceVariationManagementService priceVariationManagementService;
    @Override
    @Transactional(isolation = Isolation.DEFAULT, propagation = Propagation.REQUIRED)
    public void deleteAllByProductCatalogManagementId(Long productCatalogManagementId) {
        repository.deleteAllByProductCatalogManagementId(productCatalogManagementId);
    }

    @Override
    public List<ProductCatalogManagementMapEntity> entityList(Long idProductCatalog) {
        return repository.findAllByProductCatalogManagementId(idProductCatalog);
    }


    @Override
    public List<ProductCatalogManagementMapAttributeOutput> findAllByProductCatalogManagementId(Long idProductCatalogManager) {
       List<ProductCatalogManagementMapAttributeOutput> outputList = new ArrayList<>();
        List<ProductCatalogManagementMapEntity> entityList = repository.findAllByProductCatalogManagementId(idProductCatalogManager);
        entityList.forEach(productCatalogManagementMapEntity -> {
            ProductCatalogManagementMapAttributeOutput output = new ProductCatalogManagementMapAttributeOutput();
            output.setAttribute(propertyManagementService.getListAttribute(productCatalogManagementMapEntity.getAttributeGroupManagementId()));
            output.setNameAttributeGroup(attributeGroupManagementRepository.findGroupNameById(productCatalogManagementMapEntity.getAttributeGroupManagementId()));
            outputList.add(output);
        });
        return outputList;
    }

    @Override
    public List<ProductCatalogManagementMapPriceVariationOutput> findAllByProductCatalogManagementId1(Long idProductCatalogManager) {
        List<ProductCatalogManagementMapPriceVariationOutput> outputList = new ArrayList<>();
        List<ProductCatalogManagementMapEntity> entityList = repository.findAllByProductCatalogManagementId(idProductCatalogManager);
        entityList.forEach(entity ->{
            ProductCatalogManagementMapPriceVariationOutput output = new ProductCatalogManagementMapPriceVariationOutput();
            output.setPriceVariation(priceVariationManagementService.mapEntityToOutput(priceVariationManagementRepository.findById(entity.getPriceVariationManagementId()).get()));
            outputList.add(output);
        });
        return outputList;
    }
}
