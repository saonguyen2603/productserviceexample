package com.afs.backend.commonservice.service.productmanagement;

import com.afs.backend.commonservice.dto.productmanagement.ProductManagementInput;
import com.afs.backend.commonservice.dto.productmanagement.ProductManagementOutput;
import com.afs.backend.commonservice.entity.ProductManagementEntity;
import org.mapstruct.Mapper;

@Mapper
public interface ProductManagementMapper {
    ProductManagementEntity mapInputToEntity(ProductManagementInput input);
    ProductManagementOutput mapEntityToOutput(ProductManagementEntity entity);
}
