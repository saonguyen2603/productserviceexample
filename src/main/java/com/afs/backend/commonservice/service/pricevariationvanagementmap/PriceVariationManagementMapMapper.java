package com.afs.backend.commonservice.service.pricevariationvanagementmap;

import com.afs.backend.commonservice.dto.pricevariationmanagementmap.PriceVariationManagementMapInput;
import com.afs.backend.commonservice.dto.pricevariationmanagementmap.PriceVariationManagementMapOutput;
import com.afs.backend.commonservice.entity.PriceVariationManagementMapEntity;
import org.mapstruct.Mapper;

@Mapper
public interface PriceVariationManagementMapMapper {
    PriceVariationManagementMapEntity mapInputToEntity(PriceVariationManagementMapInput input);
    PriceVariationManagementMapOutput mapEntityToOutput(PriceVariationManagementMapEntity entity);
}
