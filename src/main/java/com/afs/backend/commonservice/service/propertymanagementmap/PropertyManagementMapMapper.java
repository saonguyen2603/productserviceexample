package com.afs.backend.commonservice.service.propertymanagementmap;

import com.afs.backend.commonservice.dto.propertymanagemententitymap.PropertyManagementMapInput;
import com.afs.backend.commonservice.dto.propertymanagemententitymap.PropertyManagementMapOutput;
import com.afs.backend.commonservice.entity.PropertyManagementEntityMap;
import org.mapstruct.Mapper;

@Mapper
public interface PropertyManagementMapMapper {
    PropertyManagementEntityMap mapInputToEntity(PropertyManagementMapInput input);
    PropertyManagementMapOutput mapEntityToOutput(PropertyManagementEntityMap entityMap);
}
