package com.afs.backend.commonservice.service.propertymanagement;

import com.afs.backend.commonservice.entity.PropertyManagementEntity;
import com.afs.backend.commonservice.dto.propertymanagement.PropertyManagementInput;
import com.afs.backend.commonservice.dto.propertymanagement.PropertyManagementOutput;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper
public interface PropertyManagementMapper {
    PropertyManagementEntity mapInputToEntity(PropertyManagementInput input);
    PropertyManagementOutput mapEntityToOutput(PropertyManagementEntity entity);
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void update(@MappingTarget PropertyManagementEntity entity, PropertyManagementInput input);
}
