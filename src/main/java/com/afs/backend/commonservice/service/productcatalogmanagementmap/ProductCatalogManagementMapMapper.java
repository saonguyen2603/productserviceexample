package com.afs.backend.commonservice.service.productcatalogmanagementmap;

import com.afs.backend.commonservice.dto.productcatalogmanagementmap.ProductCatalogManagementMapInput;
import com.afs.backend.commonservice.entity.ProductCatalogManagementMapEntity;
import org.mapstruct.Mapper;

@Mapper
public interface ProductCatalogManagementMapMapper {
    ProductCatalogManagementMapEntity mapInputToEntity(ProductCatalogManagementMapInput input);
}
