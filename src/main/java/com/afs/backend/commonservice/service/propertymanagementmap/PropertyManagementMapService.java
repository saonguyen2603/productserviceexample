package com.afs.backend.commonservice.service.propertymanagementmap;

import com.afs.backend.commonservice.dto.propertymanagemententitymap.PropertyManagementMapOutput;
import com.afs.backend.commonservice.entity.PropertyManagementEntityMap;

import java.util.List;

public interface PropertyManagementMapService {
    List<PropertyManagementEntityMap> getListEntityByPropertyManagementId(Long propertyManagementId);
    List<PropertyManagementMapOutput> getListOutputByPropertyManagementId(Long propertyManagementId);
    void deleteAllByPropertyManagementId(Long propertyManagementId);
}
