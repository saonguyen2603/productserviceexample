package com.afs.backend.commonservice.service.propertymanagementmap;

import com.afs.backend.commonservice.dto.propertymanagemententitymap.PropertyManagementMapOutput;
import com.afs.backend.commonservice.entity.PropertyManagementEntityMap;
import com.afs.backend.commonservice.repository.PropertyManagementMapRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
@Service
public class PropertyManagementMapServiceImpl implements PropertyManagementMapService{
    @Autowired
    private PropertyManagementMapRepository repository;
    @Autowired
    private PropertyManagementMapMapper mapper;
    @Override
    public List<PropertyManagementEntityMap> getListEntityByPropertyManagementId(Long propertyManagementId) {
        return repository.findAllByPropertyManagementId(propertyManagementId);
    }
    @Override
    public List<PropertyManagementMapOutput> getListOutputByPropertyManagementId(Long propertyManagementId){
        List<PropertyManagementEntityMap> propertyManagementEntityMaps = getListEntityByPropertyManagementId(propertyManagementId);
        List<PropertyManagementMapOutput> outputList = new ArrayList<>();
        propertyManagementEntityMaps.forEach(entity ->{
            PropertyManagementMapOutput output = mapper.mapEntityToOutput(entity);
            outputList.add(output);
        });
        return outputList;
    }

    @Override
    @Transactional(isolation = Isolation.DEFAULT, propagation = Propagation.REQUIRED)
    public void deleteAllByPropertyManagementId(Long propertyManagementId) {
        repository.deleteAllByPropertyManagementId(propertyManagementId);
    }
}
