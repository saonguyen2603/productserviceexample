package com.afs.backend.commonservice.service.trademark;

import com.afs.backend.commonservice.entity.TrademarkEntity;
import com.afs.backend.commonservice.dto.trademark.TrademarkInput;
import com.afs.backend.commonservice.dto.trademark.TrademarkOutput;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper
public interface TrademarkMapper {
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void update(@MappingTarget TrademarkEntity entity, TrademarkInput input);

    TrademarkEntity mapInputToEntity(TrademarkInput input);

    TrademarkOutput mapEntityToOutput(TrademarkEntity entity);
}

