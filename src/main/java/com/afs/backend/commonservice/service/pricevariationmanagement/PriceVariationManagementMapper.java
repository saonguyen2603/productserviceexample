package com.afs.backend.commonservice.service.pricevariationmanagement;

import com.afs.backend.commonservice.dto.pricevariationmanagement.PriceVariationManagementInput;
import com.afs.backend.commonservice.dto.pricevariationmanagement.PriceVariationManagementOutput;
import com.afs.backend.commonservice.entity.PriceVariationManagementEntity;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper
public interface PriceVariationManagementMapper {
    PriceVariationManagementEntity mapInputToEntity(PriceVariationManagementInput input);
    PriceVariationManagementOutput mapEntityToOutput(PriceVariationManagementEntity entity);
    void update(@MappingTarget PriceVariationManagementEntity entity, PriceVariationManagementInput input);
}
