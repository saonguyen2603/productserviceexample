package com.afs.backend.commonservice.service.productmanagementmappicturevariation;

import com.afs.backend.commonservice.dto.productmanagementmappicturevariation.ProductManagementMapPictureVariationOutput;
import com.afs.backend.commonservice.entity.ProductManagementMapPictureVariationEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductManagementMapPictureVariationServiceImpl implements ProductManagementMapPictureVariationService{
    @Override
    public List<ProductManagementMapPictureVariationOutput> getOutputLinkImages(List<ProductManagementMapPictureVariationEntity> entityList) {
        List<ProductManagementMapPictureVariationOutput> outputList = new ArrayList<>();
        entityList.forEach(entity ->{
            ProductManagementMapPictureVariationOutput output = new ProductManagementMapPictureVariationOutput();
            output.setLinkImage(entity.getLinkPictureVariation());
            outputList.add(output);
        });
        return outputList;
    }
}
