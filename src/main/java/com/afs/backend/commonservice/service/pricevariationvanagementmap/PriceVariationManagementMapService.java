package com.afs.backend.commonservice.service.pricevariationvanagementmap;

import com.afs.backend.commonservice.dto.pricevariationmanagementmap.PriceVariationManagementMapOutput;

import java.util.List;


public interface PriceVariationManagementMapService {
    List<PriceVariationManagementMapOutput> getListOutputByPriceVariationManagementId(Long priceVariationManagementId);
    void deleteAllByPriceVariationManagementId(Long priceVariationManagementId);
}
