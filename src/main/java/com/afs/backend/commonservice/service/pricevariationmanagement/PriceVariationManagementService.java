package com.afs.backend.commonservice.service.pricevariationmanagement;

import com.afs.backend.commonservice.dto.pricevariationmanagement.PriceVariationManagementInput;
import com.afs.backend.commonservice.dto.pricevariationmanagement.PriceVariationManagementOutput;
import com.afs.backend.commonservice.entity.PriceVariationManagementEntity;
import com.afs.backend.commonservice.utils.ListPageResponse;
import org.springframework.data.domain.Page;

import java.util.Collection;
import java.util.Map;

public interface PriceVariationManagementService {
    PriceVariationManagementEntity create(PriceVariationManagementInput input);
    PriceVariationManagementEntity detail(Long id);
    PriceVariationManagementOutput getOutputByEntity(PriceVariationManagementEntity entity);
    PriceVariationManagementOutput mapEntityToOutput(PriceVariationManagementEntity entity);
    void deleteById(Long id);
    PriceVariationManagementEntity update(Long id, PriceVariationManagementInput input);
    Page<PriceVariationManagementEntity> getListPriceVariationManagementEntity(String lang, Map<String, String> urlParam);

    ListPageResponse<PriceVariationManagementOutput> getListPriceVariationManagementOutput(Page<PriceVariationManagementEntity> priceVariationManagementEntities);
    Map<Long, PriceVariationManagementEntity> mapAllByIdIn(Collection<Long> ids);
}
