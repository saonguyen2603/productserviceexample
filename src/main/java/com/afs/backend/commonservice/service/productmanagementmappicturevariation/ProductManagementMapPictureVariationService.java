package com.afs.backend.commonservice.service.productmanagementmappicturevariation;

import com.afs.backend.commonservice.dto.productmanagementmappicturevariation.ProductManagementMapPictureVariationOutput;
import com.afs.backend.commonservice.entity.ProductManagementMapPictureVariationEntity;

import java.util.List;

public interface ProductManagementMapPictureVariationService {
    List<ProductManagementMapPictureVariationOutput> getOutputLinkImages(List<ProductManagementMapPictureVariationEntity> entityList);
}
