package com.afs.backend.commonservice.service.pricevariationvanagementmap;

import com.afs.backend.commonservice.dto.pricevariationmanagementmap.PriceVariationManagementMapOutput;
import com.afs.backend.commonservice.entity.PriceVariationManagementMapEntity;
import com.afs.backend.commonservice.repository.PriceVariationManagementMapRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class PriceVariationManagementMapServiceImpl implements PriceVariationManagementMapService{
    @Autowired
    private PriceVariationManagementMapRepository repository;
    @Autowired
    private PriceVariationManagementMapMapper mapper;
    @Override
    public List<PriceVariationManagementMapOutput> getListOutputByPriceVariationManagementId(Long priceVariationManagementId) {
        List<PriceVariationManagementMapOutput> outputList = new ArrayList<>();
        List<PriceVariationManagementMapEntity> entityList = repository.findAllByPriceVariationManagementId(priceVariationManagementId);
        entityList.forEach(entity -> {
            PriceVariationManagementMapOutput output = mapper.mapEntityToOutput(entity);
            outputList.add(output);
        });
        return outputList;
    }

    @Override
    @Transactional(isolation = Isolation.DEFAULT, propagation = Propagation.REQUIRED)
    public void deleteAllByPriceVariationManagementId(Long priceVariationManagementId) {
        repository.deleteAllByPriceVariationManagementId(priceVariationManagementId);
    }
}
