package com.afs.backend.commonservice.service.productcatalogmanagement;

import com.afs.backend.commonservice.dto.pricevariationmanagement.PriceVariationManagementOutput;
import com.afs.backend.commonservice.dto.productcatalogmanagement.ProductCatalogManagementInput;
import com.afs.backend.commonservice.dto.productcatalogmanagement.ProductCatalogManagementOutput;
import com.afs.backend.commonservice.dto.productcatalogmanagement.ProductCatalogManagementOutputForPriceVariation;
import com.afs.backend.commonservice.dto.propertymanagement.PropertyManagementOutput;
import com.afs.backend.commonservice.entity.ProductCatalogManagementEntity;
import com.afs.backend.commonservice.dto.productcatalogmanagement.ProductCatalogManagementOutputForAttribute;
import com.afs.backend.commonservice.utils.ListPageResponse;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

public interface ProductCatalogManagementService {
    ProductCatalogManagementEntity create (ProductCatalogManagementInput input, MultipartFile file) throws IOException;
    ProductCatalogManagementEntity detail(Long id);
    Map<Long, ProductCatalogManagementEntity> mapAllByIdIn(Collection<Long> ids);
    void delete(Long id) throws IOException;
    ProductCatalogManagementEntity update(Long id, ProductCatalogManagementInput input, MultipartFile file) throws IOException;
    ProductCatalogManagementOutput getOutputByEntity(ProductCatalogManagementEntity entity);
    Page<ProductCatalogManagementEntity> getListProductCatalogManagementEntity(String lang, Map<String, String> urlParam);
    ListPageResponse<ProductCatalogManagementOutput> getListProductCatalogManagementOutput(Page<ProductCatalogManagementEntity> propertyManagementEntityPage);
    ProductCatalogManagementOutputForAttribute getAttributeGroupList(Long id);
    PropertyManagementOutput detailAttribute(String code);
    ProductCatalogManagementOutputForPriceVariation getPriceVariationList(Long id);
    PriceVariationManagementOutput detailPriceVariation(String code);
}
