package com.afs.backend.commonservice.service.productmanagementpicture;

import com.afs.backend.commonservice.dto.productmanagementpicture.ProductManagementPictureOutput;
import com.afs.backend.commonservice.entity.ProductManagementPictureEntity;
import com.afs.backend.commonservice.repository.ProductManagementPictureRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
@AllArgsConstructor
public class ProductManagementPictureServiceImpl implements ProductManagementPictureService {
    private final ProductManagementPictureRepository repository;



    public List<ProductManagementPictureOutput> getLinkImages(Collection<Long> ids) {
        List<ProductManagementPictureEntity> entityList = repository.findAllByIdIn(ids);
        List<ProductManagementPictureOutput> outputList = new ArrayList<>();
        entityList.forEach(entity ->{
            ProductManagementPictureOutput output = new ProductManagementPictureOutput();
            output.setImageLink(entity.getProductPictureLink());
            outputList.add(output);
        });
        return outputList;
    }

    @Override
    public List<ProductManagementPictureOutput> getOutputImages(List<ProductManagementPictureEntity> entityList) {
        List<ProductManagementPictureOutput> outputList = new ArrayList<>();
        entityList.forEach(entity -> {
            ProductManagementPictureOutput output = new ProductManagementPictureOutput();
            output.setImageLink(entity.getProductPictureLink());
            outputList.add(output);
        });
        return outputList;
    }
}
