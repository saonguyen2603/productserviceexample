package com.afs.backend.commonservice.service.trademark;

import com.afs.backend.commonservice.entity.TrademarkEntity;
import com.afs.backend.commonservice.dto.trademark.TrademarkInput;
import com.afs.backend.commonservice.dto.trademark.TrademarkOutput;
import com.afs.backend.commonservice.utils.ListPageResponse;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

public interface TrademarkService {
    TrademarkEntity create(TrademarkInput input, MultipartFile file) throws IOException;
    TrademarkEntity detail(Long id);
    void deleteById(Long id) throws IOException;
    TrademarkEntity update(Long id, TrademarkInput input, MultipartFile file) throws IOException;
    Page<TrademarkEntity> getListTrademarkEntity(String lang, Map<String, String> urlParam);
    ListPageResponse<TrademarkOutput> getListTrademarkOutput(Page<TrademarkEntity> trademarkEntities);
}
