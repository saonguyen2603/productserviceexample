package com.afs.backend.commonservice.service.attributegroupmanagement;

import com.afs.backend.base.common.exception.CommandExceptionBuilder;
import com.afs.backend.commonservice.dto.attributegroupmanagement.AttributeGroupManagementInput;
import com.afs.backend.commonservice.dto.attributegroupmanagement.AttributeGroupManagementOutput;
import com.afs.backend.commonservice.entity.AttributeGroupManagementEntity;
import com.afs.backend.commonservice.repository.AttributeGroupManagementRepository;
import com.afs.backend.commonservice.specifications.CommonSpecification;
import com.afs.backend.commonservice.utils.ErrorCodes;
import com.afs.backend.commonservice.validator.AttributeGroupManagementValidate;
import com.afs.backend.commonservice.utils.ListPageResponse;
import com.afs.backend.commonservice.utils.PageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;


@Service
public class AttributeGroupManagementServiceImpl implements AttributeGroupManagementService {
    @Autowired
    private AttributeGroupManagementRepository repository;
    @Autowired
    private AttributeGroupManagementMapper mapper;
    @Autowired
    private AttributeGroupManagementValidate validate;

    @Override
    public AttributeGroupManagementEntity create(AttributeGroupManagementInput input) {
        validate.validateCreate(input);
        AttributeGroupManagementEntity entity = mapper.mapInputToEntity(input);
        return repository.save(entity);
    }

    @Override
    public AttributeGroupManagementEntity detail(Long id) {
        Optional<AttributeGroupManagementEntity> optional = repository.findById(id);
        return optional.orElseThrow(() -> CommandExceptionBuilder.exception(ErrorCodes.ATTRIBUTE_GROUP_MANAGEMENT_ID_NOT_EXIXST));
    }

    @Override
    public void delete(Long id) {
        validate.validateDelete(detail(id));
        repository.deleteById(id);
    }

    @Override
    public AttributeGroupManagementEntity update(Long id, AttributeGroupManagementInput input) {
        AttributeGroupManagementEntity entity = repository.findById(id).get();
        validate.validateUpdate(entity, input);
        mapper.update(entity, input);
        return repository.save(entity);
    }

    @Override
    public Page<AttributeGroupManagementEntity> getListAttributeGroupManagementEntity(String lang, Map<String, String> urlParam) {
        Specification<AttributeGroupManagementEntity> conditions = Specification.where(
                new CommonSpecification<AttributeGroupManagementEntity>()
                        .withFilter(null, AttributeGroupManagementEntity.searchField(), urlParam, lang)
        );
        return repository.findAll(conditions, PageUtils.getPageFromUrl(urlParam));
    }

    @Override
    public AttributeGroupManagementOutput getEntityToOutput(AttributeGroupManagementEntity entity) {
        return mapper.mapEntityToOutput(entity);
    }


    @Override
    public ListPageResponse<AttributeGroupManagementOutput> getListAttributeGroupManagementOutput(Page<AttributeGroupManagementEntity> attributeGroupManagementEntityPage) {
        return PageUtils.formatPageResponse(attributeGroupManagementEntityPage.map(this::getEntityToOutput));
    }

    @Override
    public Map<Long, AttributeGroupManagementEntity> mapAllEntityByIds(Collection<Long> ids) {
        List<AttributeGroupManagementEntity> groupManagementEntityList = repository.findAllByIdIn(ids);
        return groupManagementEntityList.stream().collect(Collectors.toMap(AttributeGroupManagementEntity::getId, Function.identity()));
    }
}
