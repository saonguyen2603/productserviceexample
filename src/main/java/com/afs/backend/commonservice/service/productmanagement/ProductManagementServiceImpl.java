package com.afs.backend.commonservice.service.productmanagement;

import com.afs.backend.base.common.exception.CommandExceptionBuilder;
import com.afs.backend.commonservice.dto.pageproductattribute.PageProductAttributeOutput;
import com.afs.backend.commonservice.dto.productmanagement.ProductManagementInput;
import com.afs.backend.commonservice.dto.productmanagement.ProductManagementOutput;
import com.afs.backend.commonservice.dto.productmanagementmapproperty.ProductManagementMapPropertyDetailAndValueOutput;
import com.afs.backend.commonservice.dto.productmanagementmapproperty.ProductManagementMapPropertyDetailOutput;
import com.afs.backend.commonservice.dto.productmanagementmapproperty.ProductManagementMapPropertyInput;
import com.afs.backend.commonservice.dto.productmanagementmapproperty.ProductManagementMapPropertyOutput;
import com.afs.backend.commonservice.dto.productmanagementmapvariationinput.ProductManagementMapVariationOutput;
import com.afs.backend.commonservice.entity.*;
import com.afs.backend.commonservice.repository.*;
import com.afs.backend.commonservice.service.attributegroupmanagement.AttributeGroupManagementService;
import com.afs.backend.commonservice.service.pricevariationmanagement.PriceVariationManagementService;
import com.afs.backend.commonservice.service.productcatalogmanagement.ProductCatalogManagementService;
import com.afs.backend.commonservice.service.productmanagementmappicturevariation.ProductManagementMapPictureVariationService;
import com.afs.backend.commonservice.service.productmanagementpicture.ProductManagementPictureService;
import com.afs.backend.commonservice.service.propertymanagement.PropertyManagementService;
import com.afs.backend.commonservice.specifications.CommonSpecification;
import com.afs.backend.commonservice.utils.ErrorCodes;
import com.afs.backend.commonservice.utils.ListPageResponse;
import com.afs.backend.commonservice.utils.PageUtils;
import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.google.common.collect.Sets;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ProductManagementServiceImpl implements ProductManagementService {
    private final ProductManagementMapper mapper;
    private final ProductManagementRepository repository;
    private final ProductManagementMapPropertyRepository mapPropertyRepository;
    private final Cloudinary cloudinary;
    private final ProductManagementMapVariationRepository mapVariationRepository;
    private final ProductManagementMapPictureVariationRepository productManagementMapPictureVariationRepository;
    private final ProductManagementPictureRepository productManagementPictureRepository;
    private final ProductCatalogManagementService productCatalogManagementService;
    private final PriceVariationManagementService priceVariationManagementService;
    private final ProductManagementPictureService managementPictureService;
    private final ProductManagementMapPictureVariationService productManagementMapPictureVariationService;
    private final AttributeGroupManagementService attributeGroupManagementService;
    private final PropertyManagementService propertyManagementService;
    private final PageProductAttributeRepository pageProductAttributeRepository;

    @Override
    public ProductManagementEntity create(ProductManagementInput input, MultipartFile[] imageProduct, List<MultipartFile[]> imageVariationProduct) throws IOException {
        ProductManagementEntity entity = mapper.mapInputToEntity(input);
        repository.save(entity);
        if (input.getInformationProperties()) {
            Set<ProductManagementMapPropertyEntity> mapPropertyEntityList = new HashSet<>();
            for (ProductManagementMapPropertyInput mapInput : input.getMapPropertyInputList()) {
                ProductManagementMapPropertyEntity mapPropertyEntity = new ProductManagementMapPropertyEntity();
                mapPropertyEntity.setProductManagementId(entity.getId());
                mapPropertyEntity.setAttributeGroupManagementId(mapInput.getAttributeGroupManagementId());
                mapPropertyEntity.setPropertyManagementId(mapInput.getPropertyManagementId());
                mapPropertyEntity.setValue(mapInput.getValue());
                mapPropertyEntityList.add(mapPropertyEntity);
            }
            mapPropertyRepository.saveAll(mapPropertyEntityList);
        }
        if (input.getPriceVariation()) {
            for (int i = 0; i < input.getMapVariationInputList().size(); i++) {
                ProductManagementMapVariationEntity mapVariationEntity = new ProductManagementMapVariationEntity();
                mapVariationEntity.setProductManagementId(entity.getId());
                mapVariationEntity.setPriceVariationManagementId(input.getMapVariationInputList().get(i).getPriceVariationManagementId());
                mapVariationEntity.setCodeProduct(input.getMapVariationInputList().get(i).getCodeProduct());
                mapVariationRepository.save(mapVariationEntity);
                for (MultipartFile file : imageVariationProduct.get(i)) {
                    Map uploadResult = cloudinary.uploader().upload(file.getBytes(), ObjectUtils.emptyMap());
                    String url = uploadResult.get("url").toString();
                    String uploadId = (String) uploadResult.get("public_id");
                    ProductManagementMapPictureVariationEntity managementMapPictureVariationEntity = new ProductManagementMapPictureVariationEntity();
                    managementMapPictureVariationEntity.setProductManagementMapVariationId(mapVariationEntity.getId());
                    managementMapPictureVariationEntity.setLinkPictureVariation(url);
                    managementMapPictureVariationEntity.setPublicIdPictureVariation(uploadId);
                    productManagementMapPictureVariationRepository.save(managementMapPictureVariationEntity);
                }
            }
        }
        List<ProductManagementPictureEntity> entitySet = new ArrayList<>();
        for (MultipartFile file : imageProduct) {
            Map uploadResult = cloudinary.uploader().upload(file.getBytes(), ObjectUtils.emptyMap());
            String url = uploadResult.get("url").toString();
            String uploadId = (String) uploadResult.get("public_id");
            ProductManagementPictureEntity productManagementPictureEntity = new ProductManagementPictureEntity();
            productManagementPictureEntity.setProductManagementId(entity.getId());
            productManagementPictureEntity.setPublicIdProductPicture(uploadId);
            productManagementPictureEntity.setProductPictureLink(url);
            entitySet.add(productManagementPictureEntity);
        }
        productManagementPictureRepository.saveAll(entitySet);
        return entity;
    }

    @Override
    public ProductManagementEntity detail(Long id) {
        Optional<ProductManagementEntity> optional = repository.findById(id);
        return optional.orElseThrow(() -> CommandExceptionBuilder.exception(ErrorCodes.PRODUCT_MANAGEMENT_ID_NOT_EXIXST));

    }

    @Override
    public ProductManagementOutput getEntityToOutput(ProductManagementEntity entity) {
        Map<Long, ProductCatalogManagementEntity> managementEntityMap = productCatalogManagementService.mapAllByIdIn(Sets.newHashSet(entity.getProductCatalogManagementId()));
        ProductManagementOutput output = mapper.mapEntityToOutput(entity);
        output.setProductCatalogManagement(productCatalogManagementService.getOutputByEntity(managementEntityMap.get(entity.getProductCatalogManagementId())));
        return output;
    }

    @Override
    public Page<ProductManagementEntity> getListProductManagementEntity(String lang, Map<String, String> urlParam) {
        Specification<ProductManagementEntity> conditions = Specification.where(
                new CommonSpecification<ProductManagementEntity>()
                        .withFilter(null, ProductManagementEntity.searchField(), urlParam, lang)
        );
        return repository.findAll(conditions, PageUtils.getPageFromUrl(urlParam));
    }

    @Override
    public ListPageResponse<ProductManagementOutput> getListProductManagementOutput(Page<ProductManagementEntity> managementEntityPage) {
        Set<Long> IdSet = new HashSet<>();
        Set<Long> idSet1 = new HashSet<>();
        managementEntityPage.forEach(productManagementEntity -> {
            //lấy toàn bộ id của product catalog
            IdSet.add(productManagementEntity.getProductCatalogManagementId());
            //lấy toàn bộ id của product
            idSet1.add(productManagementEntity.getId());
        });

        Map<Long, ProductCatalogManagementEntity> mapAllByProductCatalog = productCatalogManagementService.mapAllByIdIn(IdSet);
        Map<Long, List<ProductManagementPictureEntity>> mapAllManagementPicture = productManagementPictureRepository
                .findAllByProductManagementIdIn(idSet1)
                .stream()
                .collect(Collectors.groupingBy(ProductManagementPictureEntity::getProductManagementId));

        return PageUtils.formatPageResponse(managementEntityPage.map(productManagementEntity -> {
            ProductManagementOutput output = mapper.mapEntityToOutput(productManagementEntity);
            output.setProductCatalogManagement(productCatalogManagementService.getOutputByEntity(mapAllByProductCatalog.get(productManagementEntity.getProductCatalogManagementId())));
            output.setLinkImage(managementPictureService.getOutputImages(mapAllManagementPicture.get(productManagementEntity.getId())));
            return output;
        }));
    }

    @Override
    public Page<ProductManagementMapVariationEntity> getListProductManagementVariationEntity(String lang, Map<String, String> urlParam) {
        Specification<ProductManagementMapVariationEntity> conditions = Specification.where(
                new CommonSpecification<ProductManagementMapVariationEntity>()
                        .withFilter(null, ProductManagementMapVariationEntity.searchField(), urlParam, lang)
        );
        return mapVariationRepository.findAll(conditions, PageUtils.getPageFromUrl(urlParam));
    }

    @Override
    public ListPageResponse<ProductManagementMapVariationOutput> getListProductManagementVariationOutput(Page<ProductManagementMapVariationEntity> productManagementMapVariationEntities) {
        //lấy id của product, pricevariation, productmanagement_map_variation
        Set<Long> idSet = new HashSet<>();
        Set<Long> idSet1 = new HashSet<>();
        Set<Long> idSet2 = new HashSet<>();
        productManagementMapVariationEntities.forEach(productManagementMapVariationEntity -> {
            idSet.add(productManagementMapVariationEntity.getProductManagementId());
            idSet1.add(productManagementMapVariationEntity.getPriceVariationManagementId());
            idSet2.add(productManagementMapVariationEntity.getId());
        });
//        //lấy id của catalog bằng product
//        Set<Long> idSet3 = new HashSet<>();
//        List<ProductManagementEntity> productManagementEntityList = repository.findAllByIdIn(idSet);
//        productManagementEntityList.forEach(productManagementEntity->{
//            idSet3.add(productManagementEntity.getProductCatalogManagementId());
//        });

        //map các giá trị liên quan
        Map<Long, ProductManagementEntity> managementEntityMap = repository.findAllByIdIn(idSet).stream().collect(Collectors.toMap(ProductManagementEntity::getId, Function.identity()));
        Map<Long, PriceVariationManagementEntity> priceVariationManagementEntityMap = priceVariationManagementService.mapAllByIdIn(idSet1);
        Map<Long, List<ProductManagementMapPictureVariationEntity>> productManagementMapPictureVariationEntityMap = productManagementMapPictureVariationRepository
                .findAllByProductManagementMapVariationIdIn(idSet2)
                .stream()
                .collect(Collectors.groupingBy(ProductManagementMapPictureVariationEntity::getProductManagementMapVariationId));
//        Map<Long, ProductCatalogManagementEntity> productCatalogManagementEntityMap = productCatalogManagementService.mapAllByIdIn(idSet3);

        //phân trang
        return PageUtils.formatPageResponse(productManagementMapVariationEntities.map(productManagementMapVariationEntity -> {
            ProductManagementMapVariationOutput mapVariationOutput = new ProductManagementMapVariationOutput();
            mapVariationOutput.setCodeProduct(productManagementMapVariationEntity.getCodeProduct());
            mapVariationOutput.setProductManagement
                    (getEntityToOutput(managementEntityMap.get(productManagementMapVariationEntity.getProductManagementId())));
            mapVariationOutput.setPriceVariationManagement(priceVariationManagementService
                    .mapEntityToOutput(priceVariationManagementEntityMap.get(productManagementMapVariationEntity.getPriceVariationManagementId())));
            mapVariationOutput.setLinkImageVariation(productManagementMapPictureVariationService
                    .getOutputLinkImages(productManagementMapPictureVariationEntityMap.get(productManagementMapVariationEntity.getId())));
//            mapVariationOutput.setProductCatalog(productCatalogManagementService
//                    .getOutputByEntity(productCatalogManagementEntityMap.get(detail(productManagementMapVariationEntity.getProductManagementId()).getProductCatalogManagementId())));

            return mapVariationOutput;
        }));
    }

    @Override
    public void deleteByCode(String code) {
        repository.deleteByCode(code);
    }

    @Override
    public ProductManagementMapPropertyOutput detailProperty(String code) {
        ProductManagementEntity productManagementEntity = repository.findByCode(code);
        List<ProductManagementMapPropertyEntity> entityList = mapPropertyRepository.findAllByProductManagementId(productManagementEntity.getId());
        List<ProductManagementMapPropertyDetailOutput> outputList = new ArrayList<>();
        List<ProductManagementMapPropertyDetailAndValueOutput> valueOutputs = new ArrayList<>();
        entityList.forEach(entity ->{
            ProductManagementMapPropertyDetailAndValueOutput detailAndValueOutput = new ProductManagementMapPropertyDetailAndValueOutput();
            detailAndValueOutput.setValue(entity.getValue());
            detailAndValueOutput.setPropertyType(propertyManagementService.detail(entity.getPropertyManagementId()).getAttributeType().getValue());
            detailAndValueOutput.setNameProperty(propertyManagementService.detail(entity.getPropertyManagementId()).getName());
            valueOutputs.add((detailAndValueOutput));
        });
        entityList.forEach(entity ->{
            ProductManagementMapPropertyDetailOutput output = new ProductManagementMapPropertyDetailOutput();
            output.setNamePropertyGroup(attributeGroupManagementService.detail(entity.getAttributeGroupManagementId()).getName());
            output.setPropertyList(valueOutputs);
            outputList.add(output);
        });

        ProductManagementMapPropertyOutput output = new ProductManagementMapPropertyOutput();
        output.setNameProduct("Thuộc tính thông tin " +productManagementEntity.getName());
        output.setPropertyGroupList(outputList);
        return output;
    }

    @Override
    public Page<PageProductAttributeEntity> getListPageProductAttributeEntity(String lang, Map<String, String> urlParam) {
        return pageProductAttributeRepository.getAllByTables(PageUtils.getPageFromUrlNoSort(urlParam));
    }

    @Override
    public ListPageResponse<PageProductAttributeOutput> getListPageProductAttributeOutput(Page<PageProductAttributeEntity> productAttributeEntities) {
        // lấy toàn bộ id của PageProductAttributeEntity
        Set<Long> idSet = new HashSet<>();
        productAttributeEntities.forEach(pageProductAttribute -> idSet.add(pageProductAttribute.getId()));

        /* map giá trị của  List<ProductManagementMapPictureVariationEntity> theo ProductManagementMapVariationId
         vì id của PageProductAttributeEntity chính là lấy từ ProductManagementMapVariationId */
        Map<Long, List<ProductManagementMapPictureVariationEntity>> productManagementMapPictureVariationEntityMap = productManagementMapPictureVariationRepository
                .findAllByProductManagementMapVariationIdIn(idSet)
                .stream()
                .collect(Collectors.groupingBy(ProductManagementMapPictureVariationEntity::getProductManagementMapVariationId));

        //phân trang
        return PageUtils.formatPageResponse(productAttributeEntities.map(pageProductAttributeEntity -> {
            PageProductAttributeOutput output = new PageProductAttributeOutput();
            output.setUnit(pageProductAttributeEntity.getUnit());
            output.setUses(pageProductAttributeEntity.getUses());
            output.setNameCatalog(pageProductAttributeEntity.getNameProduct());
            output.setCodeProduct(pageProductAttributeEntity.getCodeProduct());
            output.setNameProduct(pageProductAttributeEntity.getNameProduct());
            output.setListImage(productManagementMapPictureVariationService
                    .getOutputLinkImages(productManagementMapPictureVariationEntityMap.get(pageProductAttributeEntity.getId())));
            return output;
        }));
    }

    @Override
    public ProductManagementMapPropertyOutput detailProperty1(String code) {
        ProductManagementEntity productManagementEntity = repository.findByCode(code);
        return null;
    }

    @Override
    public ProductManagementMapPropertyDetailOutput detailPropertyGroup() {
        return null;
    }
}
