package com.afs.backend.commonservice.service.productcatalogmanagementmap;

import com.afs.backend.commonservice.dto.productcatalogmanagementmap.ProductCatalogManagementMapPriceVariationOutput;
import com.afs.backend.commonservice.entity.ProductCatalogManagementMapEntity;
import com.afs.backend.commonservice.dto.productcatalogmanagementmap.ProductCatalogManagementMapAttributeOutput;

import java.util.List;

public interface ProductCatalogManagementMapService {
    void deleteAllByProductCatalogManagementId(Long productCatalogManagementId);
    List<ProductCatalogManagementMapEntity> entityList(Long idProductCatalog);
    List<ProductCatalogManagementMapAttributeOutput> findAllByProductCatalogManagementId(Long idProductCatalogManager);
    List<ProductCatalogManagementMapPriceVariationOutput> findAllByProductCatalogManagementId1(Long idProductCatalogManager);
}
