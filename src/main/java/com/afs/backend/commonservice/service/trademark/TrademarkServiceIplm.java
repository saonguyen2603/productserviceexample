package com.afs.backend.commonservice.service.trademark;

import com.afs.backend.base.common.exception.CommandExceptionBuilder;
import com.afs.backend.commonservice.entity.TrademarkEntity;
import com.afs.backend.commonservice.utils.ErrorCodes;
import com.afs.backend.commonservice.validator.TrademarkValidator;
import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.afs.backend.commonservice.dto.trademark.TrademarkInput;
import com.afs.backend.commonservice.dto.trademark.TrademarkOutput;
import com.afs.backend.commonservice.repository.TrademarkRepository;
import com.afs.backend.commonservice.specifications.CommonSpecification;
import com.afs.backend.commonservice.utils.ListPageResponse;
import com.afs.backend.commonservice.utils.PageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;


@Service
public class TrademarkServiceIplm implements TrademarkService {
    @Autowired
    private TrademarkRepository repository;
    @Autowired
    private TrademarkMapper mapper;
    @Autowired
    private Cloudinary cloudinary;
    @Autowired
    private TrademarkValidator validator;

    @Override
    public TrademarkEntity create(TrademarkInput input, MultipartFile file) throws IOException {
        validator.validateCreate(input);
        Map uploadResult = cloudinary.uploader().upload(file.getBytes(), ObjectUtils.emptyMap());
        String url = uploadResult.get("url").toString();
        String uploadId = (String) uploadResult.get("public_id");
        TrademarkEntity entity = mapper.mapInputToEntity(input);
        entity.setLogo(url);
        entity.setPublicIdImage(uploadId);
        return repository.save(entity);
    }

    @Override
    public TrademarkEntity detail(Long id) {
        Optional<TrademarkEntity> optional = repository.findById(id);
        return optional.orElseThrow(() -> CommandExceptionBuilder.exception(ErrorCodes.TRADEMARK_ID_NOT_EXISTS));
    }

    @Override
    @Transactional
    public void deleteById(Long id) throws IOException {
        // xoá ảnh trong cloudinary
        Map deleteParams = ObjectUtils.asMap("invalidate", true);
        cloudinary.uploader().destroy(detail(id).getPublicIdImage(), deleteParams);
        //xoá entity
        repository.deleteById(id);
    }

    @Override
    public TrademarkEntity update(Long id, TrademarkInput input, MultipartFile file) throws IOException {
        TrademarkEntity entity = detail(id);
        validator.validateUpdate(entity, input);
        mapper.update(entity, input);
        if (!file.isEmpty()) {
            //thay thế bằng public_id
            cloudinary.uploader().upload(file.getBytes(), ObjectUtils.asMap(
                    "public_id", entity.getPublicIdImage(),
                    "overwrite", true));
        }
        return repository.save(entity);
    }

    @Override
    public Page<TrademarkEntity> getListTrademarkEntity(String lang, Map<String, String> urlParam) {
        Specification<TrademarkEntity> conditions = Specification.where(
                new CommonSpecification<TrademarkEntity>()
                        .withFilter(null, TrademarkEntity.searchField(), urlParam, lang)
        );
        return repository.findAll(conditions, PageUtils.getPageFromUrl(urlParam));
    }

    private TrademarkOutput getEntityToOutput(TrademarkEntity entity) {
        return mapper.mapEntityToOutput(entity);
    }

    @Override
    public ListPageResponse<TrademarkOutput> getListTrademarkOutput(Page<TrademarkEntity> trademarkEntities) {
        return PageUtils.formatPageResponse(trademarkEntities.map(this::getEntityToOutput));
    }
}
