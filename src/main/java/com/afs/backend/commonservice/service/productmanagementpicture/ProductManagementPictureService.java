package com.afs.backend.commonservice.service.productmanagementpicture;
import com.afs.backend.commonservice.dto.productmanagementpicture.ProductManagementPictureOutput;
import com.afs.backend.commonservice.entity.ProductManagementPictureEntity;

import java.util.Collection;
import java.util.List;
public interface ProductManagementPictureService {
    List<ProductManagementPictureOutput> getOutputImages(List<ProductManagementPictureEntity> entityList);
}
