package com.afs.backend.commonservice.service.propertymanagement;

import com.afs.backend.commonservice.dto.propertymanagemententitymap.PropertyManagementMapInput;
import com.afs.backend.commonservice.entity.AttributeGroupManagementEntity;
import com.afs.backend.commonservice.entity.PropertyManagementEntity;
import com.afs.backend.commonservice.entity.PropertyManagementEntityMap;
import com.afs.backend.commonservice.enums.AttributeType;
import com.google.common.collect.Sets;
import com.afs.backend.commonservice.dto.propertymanagement.PropertyManagementInput;
import com.afs.backend.commonservice.dto.propertymanagement.PropertyManagementOutput;
import com.afs.backend.commonservice.repository.PropertyManagementMapRepository;
import com.afs.backend.commonservice.repository.PropertyManagementRepository;
import com.afs.backend.commonservice.service.attributegroupmanagement.AttributeGroupManagementService;
import com.afs.backend.commonservice.service.propertymanagementmap.PropertyManagementMapService;
import com.afs.backend.commonservice.specifications.CommonSpecification;
import com.afs.backend.commonservice.utils.ListPageResponse;
import com.afs.backend.commonservice.utils.PageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class PropertyManagementServiceImpl implements PropertyManagementService {
    @Autowired
    private PropertyManagementRepository repository;
    @Autowired
    private PropertyManagementMapper mapper;
    @Autowired
    private PropertyManagementMapRepository propertyManagementMapRepository;
    @Autowired
    private AttributeGroupManagementService attributeGroupManagementService;
    @Autowired
    private PropertyManagementMapService propertyManagementMapService;
    @Override
    @Transactional
    public PropertyManagementEntity create(PropertyManagementInput input) {
        PropertyManagementEntity entity = mapper.mapInputToEntity(input);
        repository.save(entity);
        if(input.getAttributeType().equals(AttributeType.CHECK_BOX.getValue())){
            List<PropertyManagementEntityMap> listEntityMap = new ArrayList<>();
            for (PropertyManagementMapInput inputMap : input.getPropertyManagementMapInputList()) {
                PropertyManagementEntityMap entityMap = new PropertyManagementEntityMap();
                entityMap.setName(inputMap.getName());
                entityMap.setAlias(inputMap.getAlias());
                entityMap.setOrder(inputMap.getOrder());
                entityMap.setDefault1(inputMap.getDefault1());
                entityMap.setPropertyManagementId(entity.getId());
                listEntityMap.add(entityMap);
            }
            propertyManagementMapRepository.saveAll(listEntityMap);
        } else {
            return entity;
        }

        return entity;
    }

    @Override
    public PropertyManagementOutput getEntityToOutput(PropertyManagementEntity entity) {
        Map<Long, AttributeGroupManagementEntity> groupManagementEntityMap = attributeGroupManagementService.mapAllEntityByIds(Sets.newHashSet(entity.getAttributeGroupManagementId()));
        PropertyManagementOutput output = mapper.mapEntityToOutput(entity);
        output.setAttributeGroupManagementId(attributeGroupManagementService.getEntityToOutput(groupManagementEntityMap.get(entity.getAttributeGroupManagementId())));
        output.setListMap(propertyManagementMapService.getListOutputByPropertyManagementId(entity.getId()));
        return output;
    }

    @Override
    public PropertyManagementEntity detail(Long id) {
        Optional<PropertyManagementEntity> optional = repository.findById(id);
        return optional.get();
    }

    @Override
    public PropertyManagementEntity update(Long id, PropertyManagementInput input) {
        PropertyManagementEntity entity = detail(id);
        mapper.update(entity, input);
        propertyManagementMapService.deleteAllByPropertyManagementId(id);
        repository.save(entity);

        if(input.getAttributeType().equals(AttributeType.CHECK_BOX.getValue())){
            List<PropertyManagementEntityMap> listEntityMap = new ArrayList<>();
            for (PropertyManagementMapInput inputMap : input.getPropertyManagementMapInputList()) {
                PropertyManagementEntityMap entityMap = new PropertyManagementEntityMap();
                entityMap.setName(inputMap.getName());
                entityMap.setAlias(inputMap.getAlias());
                entityMap.setOrder(inputMap.getOrder());
                entityMap.setDefault1(inputMap.getDefault1());
                entityMap.setPropertyManagementId(entity.getId());
                listEntityMap.add(entityMap);
            }
            propertyManagementMapRepository.saveAll(listEntityMap);
        } else {
            return entity;
        }
        return entity;
    }

    @Override
    public void deleteById(Long id) {
        propertyManagementMapService.deleteAllByPropertyManagementId(id);
        repository.deleteById(id);
    }

    @Override
    public Page<PropertyManagementEntity> getListPropertyManagementEntity(String lang, Map<String, String> urlParam) {
        Specification<PropertyManagementEntity> conditions = Specification.where(
                new CommonSpecification<PropertyManagementEntity>()
                        .withFilter(null, PropertyManagementEntity.searchField(), urlParam, lang)
        );
        return repository.findAll(conditions, PageUtils.getPageFromUrl(urlParam));
    }

    @Override
    public ListPageResponse<PropertyManagementOutput> getListPropertyManagementOutput(Page<PropertyManagementEntity> propertyManagementEntityPage) {
        Set<Long> idSet = new HashSet<>();
        propertyManagementEntityPage.forEach(infoEntity -> {
            idSet.add(infoEntity.getAttributeGroupManagementId());
        });
        Map<Long, AttributeGroupManagementEntity> mapAllAttributeEntity = attributeGroupManagementService.mapAllEntityByIds(idSet);
        return PageUtils.formatPageResponse(propertyManagementEntityPage.map(entity -> {
            PropertyManagementOutput output = mapper.mapEntityToOutput(entity);
            output.setAttributeGroupManagementId(attributeGroupManagementService.getEntityToOutput(mapAllAttributeEntity.get(entity.getAttributeGroupManagementId())));
            return output;
        }));
    }

    @Override
    public List<PropertyManagementOutput> getListAttribute(Long idGroupAttribute) {
        List<PropertyManagementOutput> outputList = new ArrayList<>();
        entityList(idGroupAttribute).forEach(entity -> {
            PropertyManagementOutput output = mapper.mapEntityToOutput(entity);
            outputList.add(output);
        });
        return outputList;
    }

    @Override
    public List<PropertyManagementEntity> entityList(Long idGroupAttribute) {
        return repository.findAllByAttributeGroupManagementId(idGroupAttribute);
    }
}
