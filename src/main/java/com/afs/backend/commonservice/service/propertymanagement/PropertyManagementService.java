package com.afs.backend.commonservice.service.propertymanagement;

import com.afs.backend.commonservice.entity.PropertyManagementEntity;
import com.afs.backend.commonservice.dto.propertymanagement.PropertyManagementInput;
import com.afs.backend.commonservice.dto.propertymanagement.PropertyManagementOutput;
import com.afs.backend.commonservice.utils.ListPageResponse;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

public interface PropertyManagementService {
    PropertyManagementEntity create(PropertyManagementInput input);
    PropertyManagementOutput getEntityToOutput(PropertyManagementEntity entity);
    PropertyManagementEntity detail(Long id);
    PropertyManagementEntity update(Long id, PropertyManagementInput input);
    void deleteById(Long id);
    Page<PropertyManagementEntity> getListPropertyManagementEntity(String lang, Map<String, String> urlParam);
    ListPageResponse<PropertyManagementOutput> getListPropertyManagementOutput(Page<PropertyManagementEntity> propertyManagementEntityPage);
    List<PropertyManagementOutput> getListAttribute(Long idGroupAttribute);
    List<PropertyManagementEntity> entityList(Long idGroupAttribute);
}
