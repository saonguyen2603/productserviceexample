package com.afs.backend.commonservice.service.pricevariationmanagement;

import com.afs.backend.base.common.exception.CommandExceptionBuilder;
import com.afs.backend.commonservice.dto.pricevariationmanagement.PriceVariationManagementInput;
import com.afs.backend.commonservice.dto.pricevariationmanagement.PriceVariationManagementOutput;
import com.afs.backend.commonservice.dto.pricevariationmanagementmap.PriceVariationManagementMapInput;
import com.afs.backend.commonservice.entity.PriceVariationManagementMapEntity;
import com.afs.backend.commonservice.repository.PriceVariationManagementMapRepository;
import com.afs.backend.commonservice.repository.PriceVariationManagementRepository;
import com.afs.backend.commonservice.utils.ErrorCodes;
import lombok.AllArgsConstructor;
import com.afs.backend.commonservice.entity.PriceVariationManagementEntity;
import com.afs.backend.commonservice.service.pricevariationvanagementmap.PriceVariationManagementMapMapper;
import com.afs.backend.commonservice.service.pricevariationvanagementmap.PriceVariationManagementMapService;
import com.afs.backend.commonservice.specifications.CommonSpecification;
import com.afs.backend.commonservice.utils.ListPageResponse;
import com.afs.backend.commonservice.utils.PageUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class PriceVariationManagementServiceImpl implements PriceVariationManagementService{
    private final PriceVariationManagementRepository repository;
    private final PriceVariationManagementMapper mapper;
    private final PriceVariationManagementMapMapper priceVariationManagementMapMapper;
    private final PriceVariationManagementMapRepository priceVariationManagementMapRepository;
    private final PriceVariationManagementMapService priceVariationManagementMapService;

    @Override
    @Transactional
    public PriceVariationManagementEntity create(PriceVariationManagementInput input) {
        PriceVariationManagementEntity entity = mapper.mapInputToEntity(input);
        repository.save(entity);
        for(PriceVariationManagementMapInput mapInput: input.getAttribute()){
            PriceVariationManagementMapEntity mapEntity = priceVariationManagementMapMapper.mapInputToEntity(mapInput);
            mapEntity.setPriceVariationManagementId(entity.getId());
            priceVariationManagementMapRepository.save(mapEntity);
        }
        return entity;
    }

    @Override
    public PriceVariationManagementEntity detail(Long id) {
        Optional<PriceVariationManagementEntity> optional = repository.findById(id);
        return optional.orElseThrow(()-> CommandExceptionBuilder.exception(ErrorCodes.PRICE_VARIATION_MANAGEMENT_ID_NOT_EXISTS));
    }

    @Override
    public PriceVariationManagementOutput getOutputByEntity(PriceVariationManagementEntity entity) {
        PriceVariationManagementOutput output = mapper.mapEntityToOutput(entity);
        output.setAttribute(priceVariationManagementMapService.getListOutputByPriceVariationManagementId(entity.getId()));
        return output;
    }

    @Override
    public PriceVariationManagementOutput mapEntityToOutput(PriceVariationManagementEntity entity) {
        return mapper.mapEntityToOutput(entity);
    }

    @Override
    public void deleteById(Long id) {
        priceVariationManagementMapService.deleteAllByPriceVariationManagementId(id);
        repository.deleteById(id);
    }

    @Override
    public PriceVariationManagementEntity update(Long id, PriceVariationManagementInput input) {
        PriceVariationManagementEntity entity = detail(id);
        mapper.update(entity, input);
        repository.save(entity);
        priceVariationManagementMapService.deleteAllByPriceVariationManagementId(id);
        for(PriceVariationManagementMapInput mapInput: input.getAttribute()){
            PriceVariationManagementMapEntity mapEntity = priceVariationManagementMapMapper.mapInputToEntity(mapInput);
            mapEntity.setPriceVariationManagementId(entity.getId());
            priceVariationManagementMapRepository.save(mapEntity);
        }
        return entity;
    }

    private PriceVariationManagementOutput mapperOutput(PriceVariationManagementEntity entity){
        return mapper.mapEntityToOutput(entity);
    }

    @Override
    public Page<PriceVariationManagementEntity> getListPriceVariationManagementEntity(String lang, Map<String, String> urlParam) {
        Specification<PriceVariationManagementEntity> conditions = Specification.where(
                new CommonSpecification<PriceVariationManagementEntity>()
                        .withFilter(null, PriceVariationManagementEntity.searchField(), urlParam, lang)
        );
        return repository.findAll(conditions, PageUtils.getPageFromUrl(urlParam));
    }

    @Override
    public ListPageResponse<PriceVariationManagementOutput> getListPriceVariationManagementOutput(Page<PriceVariationManagementEntity> priceVariationManagementEntities) {
        return PageUtils.formatPageResponse(priceVariationManagementEntities.map(this::mapperOutput));
    }

    @Override
    public Map<Long, PriceVariationManagementEntity> mapAllByIdIn(Collection<Long> ids) {
        List<PriceVariationManagementEntity> entityList = repository.findAllByIdIn(ids);
        return entityList.stream().collect(Collectors.toMap(PriceVariationManagementEntity::getId, Function.identity()));
    }


}
