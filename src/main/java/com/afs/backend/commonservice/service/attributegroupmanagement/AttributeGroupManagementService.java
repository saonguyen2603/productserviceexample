package com.afs.backend.commonservice.service.attributegroupmanagement;

import com.afs.backend.commonservice.dto.attributegroupmanagement.AttributeGroupManagementInput;
import com.afs.backend.commonservice.dto.attributegroupmanagement.AttributeGroupManagementOutput;
import com.afs.backend.commonservice.entity.AttributeGroupManagementEntity;
import com.afs.backend.commonservice.utils.ListPageResponse;
import org.springframework.data.domain.Page;

import java.util.Collection;
import java.util.Map;

public interface AttributeGroupManagementService {
    AttributeGroupManagementEntity create(AttributeGroupManagementInput input);
    AttributeGroupManagementEntity detail(Long id);
    void delete(Long id);
    AttributeGroupManagementEntity update(Long id, AttributeGroupManagementInput input);
    Page<AttributeGroupManagementEntity> getListAttributeGroupManagementEntity(String lang, Map<String, String> urlParam);
    ListPageResponse<AttributeGroupManagementOutput> getListAttributeGroupManagementOutput(Page<AttributeGroupManagementEntity> attributeGroupManagementEntityPage);
    Map<Long, AttributeGroupManagementEntity> mapAllEntityByIds(Collection<Long> ids);
    AttributeGroupManagementOutput getEntityToOutput(AttributeGroupManagementEntity entity);
}
