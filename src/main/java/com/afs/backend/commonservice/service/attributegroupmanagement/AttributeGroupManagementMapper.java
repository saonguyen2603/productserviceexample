package com.afs.backend.commonservice.service.attributegroupmanagement;

import com.afs.backend.commonservice.dto.attributegroupmanagement.AttributeGroupManagementInput;
import com.afs.backend.commonservice.dto.attributegroupmanagement.AttributeGroupManagementOutput;
import com.afs.backend.commonservice.entity.AttributeGroupManagementEntity;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper
public interface AttributeGroupManagementMapper {
    AttributeGroupManagementOutput mapEntityToOutput(AttributeGroupManagementEntity entity);
    AttributeGroupManagementEntity mapInputToEntity(AttributeGroupManagementInput input);
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void update(@MappingTarget AttributeGroupManagementEntity entity, AttributeGroupManagementInput input);
}
