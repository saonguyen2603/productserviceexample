package com.afs.backend.commonservice.service.productcatalogmanagement;

import com.afs.backend.commonservice.dto.productcatalogmanagement.ProductCatalogManagementInput;
import com.afs.backend.commonservice.dto.productcatalogmanagement.ProductCatalogManagementOutput;
import com.afs.backend.commonservice.entity.ProductCatalogManagementEntity;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper
public interface ProductCatalogManagementMapper {
    ProductCatalogManagementEntity mapInputToEntity(ProductCatalogManagementInput input);
    ProductCatalogManagementOutput mapEntityToOutput(ProductCatalogManagementEntity entity);
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void update(@MappingTarget ProductCatalogManagementEntity entity, ProductCatalogManagementInput input);
}
