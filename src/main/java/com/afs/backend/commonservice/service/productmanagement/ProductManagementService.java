package com.afs.backend.commonservice.service.productmanagement;

import com.afs.backend.commonservice.dto.pageproductattribute.PageProductAttributeOutput;
import com.afs.backend.commonservice.dto.productmanagement.ProductManagementInput;
import com.afs.backend.commonservice.dto.productmanagement.ProductManagementOutput;
import com.afs.backend.commonservice.dto.productmanagementmapproperty.ProductManagementMapPropertyDetailOutput;
import com.afs.backend.commonservice.dto.productmanagementmapproperty.ProductManagementMapPropertyOutput;
import com.afs.backend.commonservice.dto.productmanagementmapvariationinput.ProductManagementMapVariationOutput;
import com.afs.backend.commonservice.entity.PageProductAttributeEntity;
import com.afs.backend.commonservice.entity.ProductManagementEntity;
import com.afs.backend.commonservice.entity.ProductManagementMapVariationEntity;
import com.afs.backend.commonservice.utils.ListPageResponse;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface ProductManagementService {
    ProductManagementEntity create(ProductManagementInput input, MultipartFile[] imageProduct, List<MultipartFile[]> imageVariationProduct) throws IOException;
    ProductManagementEntity detail(Long id);
    ProductManagementOutput getEntityToOutput(ProductManagementEntity entity);
    Page<ProductManagementEntity> getListProductManagementEntity(String lang, Map<String, String> urlParam);
    ListPageResponse<ProductManagementOutput> getListProductManagementOutput(Page<ProductManagementEntity> managementEntityPage);
    Page<ProductManagementMapVariationEntity> getListProductManagementVariationEntity(String lang, Map<String, String> urlParam);
    ListPageResponse<ProductManagementMapVariationOutput> getListProductManagementVariationOutput(Page<ProductManagementMapVariationEntity> productManagementMapVariationEntities);
    void deleteByCode(String code);
    ProductManagementMapPropertyOutput detailProperty(String code);
    Page<PageProductAttributeEntity> getListPageProductAttributeEntity(String lang, Map<String, String> urlParam);
    ListPageResponse<PageProductAttributeOutput> getListPageProductAttributeOutput(Page<PageProductAttributeEntity> productAttributeEntities);
    ProductManagementMapPropertyOutput detailProperty1(String code);
    ProductManagementMapPropertyDetailOutput detailPropertyGroup();
}
