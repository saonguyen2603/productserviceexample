package com.afs.backend.commonservice.service.productcatalogmanagement;

import com.afs.backend.commonservice.dto.pricevariationmanagement.PriceVariationManagementOutput;
import com.afs.backend.commonservice.dto.productcatalogmanagement.ProductCatalogManagementInput;
import com.afs.backend.commonservice.dto.productcatalogmanagement.ProductCatalogManagementOutput;
import com.afs.backend.commonservice.dto.productcatalogmanagement.ProductCatalogManagementOutputForPriceVariation;
import com.afs.backend.commonservice.dto.productcatalogmanagementmap.ProductCatalogManagementMapInput;
import com.afs.backend.commonservice.dto.propertymanagement.PropertyManagementOutput;
import com.afs.backend.commonservice.entity.ProductCatalogManagementEntity;
import com.afs.backend.commonservice.entity.ProductCatalogManagementMapEntity;
import com.afs.backend.commonservice.entity.PropertyManagementEntity;
import com.afs.backend.commonservice.repository.PriceVariationManagementRepository;
import com.afs.backend.commonservice.repository.ProductCatalogManagementMapRepository;
import com.afs.backend.commonservice.repository.ProductCatalogManagementRepository;
import com.afs.backend.commonservice.service.pricevariationmanagement.PriceVariationManagementService;
import com.afs.backend.commonservice.service.propertymanagement.PropertyManagementService;
import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.afs.backend.commonservice.dto.productcatalogmanagement.ProductCatalogManagementOutputForAttribute;
import com.afs.backend.commonservice.entity.PriceVariationManagementEntity;
import com.afs.backend.commonservice.repository.PropertyManagementRepository;
import com.afs.backend.commonservice.service.productcatalogmanagementmap.ProductCatalogManagementMapService;
import com.afs.backend.commonservice.specifications.CommonSpecification;
import com.afs.backend.commonservice.utils.ListPageResponse;
import com.afs.backend.commonservice.utils.PageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class ProductCatalogManagementServiceImpl implements ProductCatalogManagementService {
    @Autowired
    private ProductCatalogManagementRepository repository;
    @Autowired
    private ProductCatalogManagementMapper mapper;
    @Autowired
    private Cloudinary cloudinary;
    @Autowired
    private ProductCatalogManagementMapRepository mapRepository;
    @Autowired
    private ProductCatalogManagementMapService productCatalogManagementMapService;
    @Autowired
    private PropertyManagementService propertyManagementService;
    @Autowired
    private PropertyManagementRepository managementRepository;
    @Autowired
    private PriceVariationManagementService priceVariationManagementService;
    @Autowired
    private PriceVariationManagementRepository priceVariationManagementRepository;

    @Override
    @Transactional
    public ProductCatalogManagementEntity create(ProductCatalogManagementInput input, MultipartFile file) throws IOException {
        ProductCatalogManagementEntity entity = mapper.mapInputToEntity(input);

        repository.save(entity);
        List<ProductCatalogManagementMapEntity> listMapEntity = new ArrayList<>();
        for(ProductCatalogManagementMapInput mapInput: input.getMapInputList()){
            ProductCatalogManagementMapEntity mapEntity = new ProductCatalogManagementMapEntity();
            mapEntity.setProductCatalogManagementId(entity.getId());
            mapEntity.setAttributeGroupManagementId(mapInput.getAttributeGroupManagementId());
            mapEntity.setPriceVariationManagementId(mapInput.getPriceVariationManagementId());
            listMapEntity.add(mapEntity);
        }
        mapRepository.saveAll(listMapEntity);
        Map uploadResult = cloudinary.uploader().upload(file.getBytes(), ObjectUtils.emptyMap());
        String url = uploadResult.get("url").toString();
        String uploadId =(String) uploadResult.get("public_id");
        entity.setPictureLink(url);
        entity.setPublicIdPicture(uploadId);
        return repository.save(entity);
    }

    @Override
    public ProductCatalogManagementEntity detail(Long id) {
        Optional<ProductCatalogManagementEntity> optional = repository.findById(id);
        return optional.get();
    }

    @Override
    public Map<Long, ProductCatalogManagementEntity> mapAllByIdIn(Collection<Long> ids) {
        List<ProductCatalogManagementEntity> entityList = repository.findAllByIdIn(ids);
        return entityList.stream().collect(Collectors.toMap(ProductCatalogManagementEntity::getId, Function.identity()));
    }

    @Override
    public void delete(Long id) throws IOException {
        Map deleteParams = ObjectUtils.asMap("invalidate", true);
        cloudinary.uploader().destroy(detail(id).getPublicIdPicture(), deleteParams);
        productCatalogManagementMapService.deleteAllByProductCatalogManagementId(id);
        repository.deleteById(id);
    }

    @Override
    public ProductCatalogManagementEntity update(Long id, ProductCatalogManagementInput input, MultipartFile file) throws IOException {
        ProductCatalogManagementEntity entity = detail(id);
        productCatalogManagementMapService.deleteAllByProductCatalogManagementId(id);
        repository.save(entity);
        List<ProductCatalogManagementMapEntity> listMapEntity = new ArrayList<>();
        for(ProductCatalogManagementMapInput mapInput: input.getMapInputList()){
            ProductCatalogManagementMapEntity mapEntity = new ProductCatalogManagementMapEntity();
            mapEntity.setProductCatalogManagementId(entity.getId());
            mapEntity.setAttributeGroupManagementId(mapInput.getAttributeGroupManagementId());
            mapEntity.setPriceVariationManagementId(mapInput.getPriceVariationManagementId());
            listMapEntity.add(mapEntity);
        }
        mapRepository.saveAll(listMapEntity);
        if(!file.isEmpty()){
            //thay thế bằng public_id (replace)
            cloudinary.uploader().upload(file.getBytes(), ObjectUtils.asMap(
                    "public_id", entity.getPublicIdPicture(),
                    "overwrite", true));
        }
        return entity;
    }

    @Override
    public ProductCatalogManagementOutput getOutputByEntity(ProductCatalogManagementEntity entity) {
        return mapper.mapEntityToOutput(entity);
    }

    @Override
    public Page<ProductCatalogManagementEntity> getListProductCatalogManagementEntity(String lang, Map<String, String> urlParam) {
        Specification<ProductCatalogManagementEntity> conditions = Specification.where(
                new CommonSpecification<ProductCatalogManagementEntity>()
                        .withFilter(null, ProductCatalogManagementEntity.searchField(), urlParam, lang)
        );
        return repository.findAll(conditions, PageUtils.getPageFromUrl(urlParam));
    }

    @Override
    public ListPageResponse<ProductCatalogManagementOutput> getListProductCatalogManagementOutput(Page<ProductCatalogManagementEntity> propertyManagementEntityPage) {
        return PageUtils.formatPageResponse(propertyManagementEntityPage.map(this::getOutputByEntity));
    }

    @Override
    public ProductCatalogManagementOutputForAttribute getAttributeGroupList(Long id) {
        ProductCatalogManagementOutputForAttribute output = new ProductCatalogManagementOutputForAttribute();
        output.setNameAttributeGroupCatalog("Nhóm Thuộc Tính" + " " + detail(id).getName());
        output.setListGroupAttribute(productCatalogManagementMapService.findAllByProductCatalogManagementId(id));
        return output;
    }

    @Override
    public PropertyManagementOutput detailAttribute(String code) {
        PropertyManagementEntity entity = managementRepository.findByCode(code);
        return propertyManagementService.getEntityToOutput(entity);
    }

    @Override
    public ProductCatalogManagementOutputForPriceVariation getPriceVariationList(Long id) {
        ProductCatalogManagementOutputForPriceVariation output = new ProductCatalogManagementOutputForPriceVariation();
        output.setNamePriceVariation("Biến Thể Giá " + detail(id).getName());
        output.setDataList(productCatalogManagementMapService.findAllByProductCatalogManagementId1(id));
        return output;
    }

    @Override
    public PriceVariationManagementOutput detailPriceVariation(String code) {
        PriceVariationManagementEntity entity = priceVariationManagementRepository.findByCode(code);
        return priceVariationManagementService.getOutputByEntity(entity);
    }


}
