package com.afs.backend.commonservice.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "product_management_map_picture_variation")
@Entity
public class ProductManagementMapPictureVariationEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "product_management_map_variation_id")
    private Long productManagementMapVariationId;
    @Column(name = "link_picture_variation")
    private String linkPictureVariation;
    @Column(name = "public_id_picture_variation")
    private String publicIdPictureVariation;
}
