package com.afs.backend.commonservice.entity;

import com.afs.backend.base.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "price_variation_management_map")
public class PriceVariationManagementMapEntity extends BaseEntity {
    @Column(name = "price_variation_management_id")
    private Long priceVariationManagementId;
    @Column(name = "name_value")
    private String nameValue;
    @Column(name = "orderNumber")
    private Long order_number;
    @Column(name = "is_default")
    private Boolean isDefault;
}
