package com.afs.backend.commonservice.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "product_management_map_property")
@Entity
public class ProductManagementMapPropertyEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "product_management_id")
    private Long productManagementId;
    @Column(name = "attribute_group_management_id")
    private Long attributeGroupManagementId;
    @Column(name = "property_management_id")
    private Long propertyManagementId;
    @Column(name = "value")
    private String value;
}
