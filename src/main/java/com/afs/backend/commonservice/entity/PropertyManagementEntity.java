package com.afs.backend.commonservice.entity;

import com.afs.backend.base.entity.BaseEntity;
import com.afs.backend.commonservice.enums.AttributeType;
import com.afs.backend.commonservice.enums.SharingModeConverter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import com.afs.backend.commonservice.enums.AttributeTypeConverter;
import com.afs.backend.commonservice.enums.SharingMode;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "property_management")
@Entity
public class PropertyManagementEntity extends BaseEntity {
    @Column(name = "code")
    private String code;
    @Column(name = "name")
    private String name;
    @Column(name = "attribute_type")
    @Convert(converter = AttributeTypeConverter.class)
    private AttributeType attributeType;
    @Column(name = "attribute_group_management_id")
    private Long attributeGroupManagementId;
    @Column(name = "sharing_mode")
    @Convert(converter = SharingModeConverter.class)
    private SharingMode sharingMode;
    @Column(name = "description")
    private String description;
    @Column(name = "price_change")
    private Boolean priceChange;
    @Column(name = "create_extra_code")
    private Boolean createExtraCode;

    public static List<String> searchField() {
        List<String> searchField = new ArrayList<>();
        searchField.add("code");
        searchField.add("name");
        searchField.add("attributeType");
        searchField.add("priceChange");
        searchField.add("sharingMode");
        searchField.add("createExtraCode");
        return searchField;
    }
}
