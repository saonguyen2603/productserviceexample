package com.afs.backend.commonservice.entity;

import com.afs.backend.base.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "product_catalog_management")
public class ProductCatalogManagementEntity extends BaseEntity {
    @Column(name = "code")
    private String code;
    @Column(name = "name")
    private String name;
    @Column(name = "picture_link")
    private String pictureLink;
    @Column(name = "public_id_picture")
    private String publicIdPicture;
    @Column(name = "description")
    private String description;

    public static List<String> searchField() {
        List<String> searchField = new ArrayList<>();
        searchField.add("code");
        searchField.add("name");
        searchField.add("pictureLink");
        searchField.add("description");
        return searchField;
    }
}
