package com.afs.backend.commonservice.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "product_catalog_management_map")
public class ProductCatalogManagementMapEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "product_catalog_management_id")
    private Long productCatalogManagementId;
    @Column(name = "price_variation_management_id")
    private Long priceVariationManagementId;
    @Column(name = "attribute_group_management_id")
    private Long attributeGroupManagementId;
}
