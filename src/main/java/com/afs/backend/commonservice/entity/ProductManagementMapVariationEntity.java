package com.afs.backend.commonservice.entity;

import com.afs.backend.base.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "product_management_map_variation")
@Entity
public class ProductManagementMapVariationEntity extends BaseEntity {

    @Column(name = "product_management_id")
    private Long productManagementId;
    @Column(name = "price_variation_management_id")
    private Long priceVariationManagementId;
    @Column(name = "code_product")
    private String codeProduct;

    public static List<String> searchField() {
        List<String> searchField = new ArrayList<>();
        searchField.add("productManagementId");
        searchField.add("priceVariationManagementId");
        searchField.add("codeProduct");
        return searchField;
    }
}
