package com.afs.backend.commonservice.entity;

import com.afs.backend.base.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "property_management_map")
@Entity
public class PropertyManagementEntityMap extends BaseEntity {
    @Column(name = "property_management_id")
    private Long propertyManagementId;
    @Column(name = "name")
    private String name;
    @Column(name = "alias")
    private String alias;
    @Column(name = "order_number")
    private Long order;
    @Column(name = "is_default")
    private Boolean default1;
}
