package com.afs.backend.commonservice.entity;

import com.afs.backend.base.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "attribute_group_management")
@Entity
public class AttributeGroupManagementEntity extends BaseEntity {
    @Column(name = "code")
    private String code;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "is_active")
    private Boolean isActive;

    public static List<String> searchField() {
        List<String> searchField = new ArrayList<>();
        searchField.add("code");
        searchField.add("name");
        searchField.add("description");
        searchField.add("is_active");
        return searchField;
    }
}
