package com.afs.backend.commonservice.entity;

import com.afs.backend.base.entity.BaseEntity;
import com.afs.backend.commonservice.enums.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "product_management")
@Entity
public class ProductManagementEntity extends BaseEntity {
    @Column(name = "code")
    private String code;
    @Column(name = "name")
    private String name;
    @Column(name = "product_catalog_management_id")
    private Long productCatalogManagementId;
    @Column(name = "producer")
    private String producer;
    @Column(name = "unit")
    @Convert(converter = UnitConverter.class)
    private Unit unit;
    @Column(name = "description")
    private String description;
    @Column(name = "uses")
    @Convert(converter = SharingModeConverter.class)
    private SharingMode uses;
    @Column(name = "batch_code_generation_support")
    private Boolean batchCodeGenerationSupport;
    @Column(name = "information_properties")
    private Boolean informationProperties;
    @Column(name = "information_properties_how_to_enter")
    private String informationPropertiesHowToEnter;
    @Column(name = "price_variation")
    private Boolean priceVariation;
    @Column(name = "price_variation_how_to_enter")
    private String priceVariationHowToEnter;
    @Column(name = "is_status")
    private Boolean isStatus;

    public static List<String> searchField() {
        List<String> searchField = new ArrayList<>();
        searchField.add("code");
        searchField.add("name");
        searchField.add("producer");
        searchField.add("unit");
        searchField.add("uses");
        return searchField;
    }
}
