package com.afs.backend.commonservice.entity;

import com.afs.backend.base.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "list")
@Entity
public class PageProductAttributeEntity extends BaseEntity implements Comparable<PageProductAttributeEntity> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "code_product")
    private String codeProduct;
    @Column(name = "name_product")
    private String nameProduct;
    @Column(name = "unit")
    private String unit;
    @Column(name = "uses")
    private String uses;
    @Column(name = "name_catalog")
    private String nameCatalog;

    public static List<String> searchField() {
        List<String> searchField = new ArrayList<>();
        searchField.add("codeProduct");
        searchField.add("nameProduct");
        searchField.add("nameCatalog");
        return searchField;
    }

    @Override
    public int compareTo(PageProductAttributeEntity o) {
        return 0;
    }

    public static class Comparators {
        public static final Comparator<PageProductAttributeEntity> CODE = (PageProductAttributeEntity o1, PageProductAttributeEntity o2) -> o1.codeProduct.compareTo(o2.codeProduct);
        public static final Comparator<PageProductAttributeEntity> NAME = (PageProductAttributeEntity o1, PageProductAttributeEntity o2) -> o1.nameProduct.compareTo(o2.nameProduct);
    }
}
