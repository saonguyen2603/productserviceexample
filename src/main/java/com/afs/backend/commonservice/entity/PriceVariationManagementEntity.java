package com.afs.backend.commonservice.entity;

import com.afs.backend.base.entity.BaseEntity;
import com.afs.backend.commonservice.enums.ColorType;
import com.afs.backend.commonservice.enums.ColorTypeConverter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "price_variation_management")
public class PriceVariationManagementEntity extends BaseEntity {
    @Column(name = "name")
    private String name;
    @Column(name = "code")
    private String code;
    @Column(name = "color_type")
    @Convert(converter = ColorTypeConverter.class)
    private ColorType colorType;

    public static List<String> searchField() {
        List<String> searchField = new ArrayList<>();
        searchField.add("code");
        searchField.add("name");
        searchField.add("color_type");
        return searchField;
    }
}
