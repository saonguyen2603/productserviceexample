package com.afs.backend.commonservice.entity;

import com.afs.backend.base.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "product_management_picture")
@Entity
public class ProductManagementPictureEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "product_management_id")
    private Long productManagementId;
    @Column(name = "product_picture_link")
    private String productPictureLink;
    @Column(name = "public_id_product_picture")
    private String publicIdProductPicture;
}
