package com.afs.backend.commonservice.repository;

import com.afs.backend.commonservice.entity.PriceVariationManagementMapEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Repository
public interface PriceVariationManagementMapRepository extends JpaRepository<PriceVariationManagementMapEntity, Long> {
    @Query(value ="select*from price_variation_management_map u where u.price_variation_management_id = ?1 ORDER BY order_number",
            nativeQuery = true)
    List<PriceVariationManagementMapEntity> findAllByPriceVariationManagementId(@PathVariable("price_variation_management_id") Long priceVariationManagementId);

    void deleteAllByPriceVariationManagementId(Long priceVariationManagementId);
}
