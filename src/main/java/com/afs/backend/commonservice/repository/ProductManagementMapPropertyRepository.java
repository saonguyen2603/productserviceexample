package com.afs.backend.commonservice.repository;

import com.afs.backend.commonservice.entity.ProductManagementMapPropertyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductManagementMapPropertyRepository extends JpaRepository<ProductManagementMapPropertyEntity, Long> {
    List<ProductManagementMapPropertyEntity> findAllByProductManagementId(Long productManagementId);
}
