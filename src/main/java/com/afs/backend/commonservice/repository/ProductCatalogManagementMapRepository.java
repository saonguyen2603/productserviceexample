package com.afs.backend.commonservice.repository;

import com.afs.backend.commonservice.entity.ProductCatalogManagementMapEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Repository
public interface ProductCatalogManagementMapRepository extends JpaRepository<ProductCatalogManagementMapEntity, Long> {
    void deleteAllByProductCatalogManagementId(Long productCatalogManagementId);

    @Query(value ="select*from product_catalog_management_map u where u.product_catalog_management_id = ?1",
            nativeQuery = true)
    List<ProductCatalogManagementMapEntity> findAllByProductCatalogManagementId(@PathVariable("product_catalog_management_id") Long productCatalogManagementId);
}
