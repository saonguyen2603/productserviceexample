package com.afs.backend.commonservice.repository;

import com.afs.backend.commonservice.entity.ProductManagementMapVariationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductManagementMapVariationRepository extends JpaRepository<ProductManagementMapVariationEntity, Long>, JpaSpecificationExecutor<ProductManagementMapVariationEntity> {

}
