package com.afs.backend.commonservice.repository;

import com.afs.backend.commonservice.entity.PropertyManagementEntityMap;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Repository
public interface PropertyManagementMapRepository extends JpaRepository<PropertyManagementEntityMap, Long> {
    @Query(value ="select*from property_management_map u where u.property_management_id = ?1 ORDER BY order_number",
            nativeQuery = true)
    List<PropertyManagementEntityMap> findAllByPropertyManagementId(@PathVariable("property_management_id") Long propertyManagementId);

    void deleteAllByPropertyManagementId(Long propertyManagementId);
}
