package com.afs.backend.commonservice.repository;

import com.afs.backend.commonservice.entity.TrademarkEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface TrademarkRepository extends JpaRepository<TrademarkEntity, Long>, JpaSpecificationExecutor<TrademarkEntity> {
    TrademarkEntity findByCode(String code);
}
