package com.afs.backend.commonservice.repository;

import com.afs.backend.commonservice.entity.PropertyManagementEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Repository
public interface PropertyManagementRepository extends JpaRepository<PropertyManagementEntity, Long>, JpaSpecificationExecutor<PropertyManagementEntity> {
    PropertyManagementEntity findByCode(String code);
    @Query(value ="select*from property_management u where u.attribute_group_management_id = ?1",
            nativeQuery = true)
    List<PropertyManagementEntity> findAllByAttributeGroupManagementId(@PathVariable("attribute_group_management_id") Long attributeGroupManagementId);
}
