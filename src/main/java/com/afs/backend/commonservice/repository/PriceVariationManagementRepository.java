package com.afs.backend.commonservice.repository;

import com.afs.backend.commonservice.entity.PriceVariationManagementEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface PriceVariationManagementRepository extends JpaRepository<PriceVariationManagementEntity, Long>, JpaSpecificationExecutor<PriceVariationManagementEntity> {
    PriceVariationManagementEntity findByCode(String code);
    List<PriceVariationManagementEntity> findAllByIdIn(Collection<Long> ids);
}
