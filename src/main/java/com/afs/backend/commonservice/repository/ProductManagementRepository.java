package com.afs.backend.commonservice.repository;

import com.afs.backend.commonservice.entity.ProductCatalogManagementEntity;
import com.afs.backend.commonservice.entity.ProductManagementEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface ProductManagementRepository extends JpaRepository<ProductManagementEntity, Long>, JpaSpecificationExecutor<ProductManagementEntity> {
     List<ProductManagementEntity> findAllByIdIn(Collection<Long> ids);
     ProductManagementEntity findByCode(String code);
     void deleteByCode(String code);
}
