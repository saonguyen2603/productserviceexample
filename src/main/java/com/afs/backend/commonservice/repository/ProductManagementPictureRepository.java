package com.afs.backend.commonservice.repository;

import com.afs.backend.commonservice.entity.ProductManagementPictureEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@Repository
public interface ProductManagementPictureRepository extends JpaRepository<ProductManagementPictureEntity, Long> {
//    List<List<ProductManagementPictureEntity>> findByProductManagementId(Long productManagementId);
    List<ProductManagementPictureEntity> findByProductManagementId(Long productManagementId);
    List<ProductManagementPictureEntity> findAllByProductManagementIdIn(Set<Long> productManagementIds);

    List<ProductManagementPictureEntity> findAllByIdIn(Collection<Long> ids);
}
