package com.afs.backend.commonservice.repository;

import com.afs.backend.commonservice.entity.PageProductAttributeEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;



@Repository
public interface PageProductAttributeRepository extends JpaRepository<PageProductAttributeEntity, Long>, JpaSpecificationExecutor<PageProductAttributeEntity> {
    @Query(value = "select a.id, a.code_product, a.created_at, a.created_by, a.updated_at, a.updated_by, d.name as name_product, b.unit, b.uses, c.name as name_catalog \n" +
            "from product_management_map_variation as a \n" +
            "left join product_management as b \n" +
            "on a.product_management_id = b.id\n" +
            "left join product_catalog_management as c \n" +
            "on c.id = b.product_catalog_management_id\n" +
            "join price_variation_management as d\n" +
            "on d.id = a.price_variation_management_id", nativeQuery = true)
    Page<PageProductAttributeEntity> getAllByTables(Pageable pageable);
}
