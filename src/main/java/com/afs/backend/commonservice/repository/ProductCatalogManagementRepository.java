package com.afs.backend.commonservice.repository;

import com.afs.backend.commonservice.entity.ProductCatalogManagementEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface ProductCatalogManagementRepository extends JpaRepository<ProductCatalogManagementEntity, Long>, JpaSpecificationExecutor<ProductCatalogManagementEntity> {
    ProductCatalogManagementEntity findByCode(String code);
    List<ProductCatalogManagementEntity> findAllByIdIn(Collection<Long> ids);
}
