package com.afs.backend.commonservice.repository;

import com.afs.backend.commonservice.entity.ProductManagementMapPictureVariationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Collection;
import java.util.List;

@Repository
public interface ProductManagementMapPictureVariationRepository extends JpaRepository<ProductManagementMapPictureVariationEntity,Long> {
    @Query(value ="select u.link_picture_variation from product_management_map_picture_variation u where u.product_management_map_variation_id = ?1",
            nativeQuery = true)
    List<String> findByProductManagementMapVariationId(@PathVariable("product_management_map_variation_id") Long productManagementMapVariationId);
    List<ProductManagementMapPictureVariationEntity> findAllByIdIn(Collection<Long> ids);

    List<ProductManagementMapPictureVariationEntity> findAllByProductManagementMapVariationIdIn(Collection<Long> productManagementMapVariationId);
}
