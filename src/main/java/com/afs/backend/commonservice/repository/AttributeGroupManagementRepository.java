package com.afs.backend.commonservice.repository;

import com.afs.backend.commonservice.entity.AttributeGroupManagementEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Collection;
import java.util.List;

@Repository
public interface AttributeGroupManagementRepository extends JpaRepository<AttributeGroupManagementEntity, Long>, JpaSpecificationExecutor<AttributeGroupManagementEntity> {
     List<AttributeGroupManagementEntity> findAllByIdIn(Collection<Long> id);

     @Query(value ="select u.name from attribute_group_management u where u.id = ?1",
             nativeQuery = true)
     String findGroupNameById(@PathVariable("id") Long id);

     AttributeGroupManagementEntity findByCode(String code);
}
