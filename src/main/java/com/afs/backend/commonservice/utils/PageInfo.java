package com.afs.backend.commonservice.utils;

public interface PageInfo {
    public static final int DEFAULT_PAGE = 0;
    public static final int PAGE_SIZE = 20;
    public static final String DEFAULT_PAGE_SORT = "created_at desc";
}
