package com.afs.backend.commonservice.utils;

import com.afs.backend.base.common.response.ErrorCode;

public interface ErrorCodes {
    ErrorCode TRADEMARK_CODE_EXISTS = new ErrorCode(1000, "error.trademark_code_exists");
    ErrorCode ATTRIBUTE_GROUP_MANAGEMENT_ID_NOT_EXIXST = new ErrorCode(1001, "error.attribute_group_management_id_not_exists");
    ErrorCode ATTRIBUTE_GROUP_MANAGEMENT_CODE_EXIXST = new ErrorCode(1002, "error.attribute_group_management_code_exists");
    ErrorCode ATTRIBUTE_GROUP_MANAGEMENT_IS_ACTIVE = new ErrorCode(1003, "error.attribute_group_management_is_active");
    ErrorCode TRADEMARK_ID_NOT_EXISTS = new ErrorCode(1004, "error.trademark_id_not_exists");
    ErrorCode PRICE_VARIATION_MANAGEMENT_ID_NOT_EXISTS = new ErrorCode(1004, "error.price_variation_management_id_not_exists");
    ErrorCode PRODUCT_MANAGEMENT_ID_NOT_EXIXST = new ErrorCode(1001, "error.attribute_group_management_id_not_exists");
}
