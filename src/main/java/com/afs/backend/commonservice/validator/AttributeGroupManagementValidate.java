package com.afs.backend.commonservice.validator;

import com.afs.backend.base.common.exception.CommandExceptionBuilder;
import com.afs.backend.commonservice.entity.AttributeGroupManagementEntity;
import com.afs.backend.commonservice.utils.ErrorCodes;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.afs.backend.commonservice.dto.attributegroupmanagement.AttributeGroupManagementInput;
import com.afs.backend.commonservice.repository.AttributeGroupManagementRepository;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@AllArgsConstructor
public class AttributeGroupManagementValidate {
    private final AttributeGroupManagementRepository repository;

    public void validateCreate(AttributeGroupManagementInput input){
        AttributeGroupManagementEntity entity = repository.findByCode(input.getCode());
        if(entity!=null){
            throw CommandExceptionBuilder.exception(ErrorCodes.ATTRIBUTE_GROUP_MANAGEMENT_CODE_EXIXST);
        }
    }

    public void validateUpdate(AttributeGroupManagementEntity entity, AttributeGroupManagementInput input){
        if(!entity.getCode().equals(input.getCode())){
            AttributeGroupManagementEntity getEntityByInputCode = repository.findByCode(input.getCode());
            if(getEntityByInputCode != null && !entity.getCode().equals(input.getCode())){
                throw CommandExceptionBuilder.exception(ErrorCodes.ATTRIBUTE_GROUP_MANAGEMENT_CODE_EXIXST);
            }
        }
    }

    public void validateDelete(AttributeGroupManagementEntity entity){
        if(entity.getIsActive()){
            throw CommandExceptionBuilder.exception(ErrorCodes.ATTRIBUTE_GROUP_MANAGEMENT_IS_ACTIVE);
        }
    }
}
