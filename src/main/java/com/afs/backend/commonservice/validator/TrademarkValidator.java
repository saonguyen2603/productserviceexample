package com.afs.backend.commonservice.validator;

import com.afs.backend.base.common.exception.CommandExceptionBuilder;
import com.afs.backend.commonservice.entity.TrademarkEntity;
import com.afs.backend.commonservice.utils.ErrorCodes;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.afs.backend.commonservice.dto.trademark.TrademarkInput;
import com.afs.backend.commonservice.repository.TrademarkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@AllArgsConstructor
public class TrademarkValidator {
    private final TrademarkRepository repository;

    public void validateCreate(TrademarkInput input){
        TrademarkEntity entity = repository.findByCode(input.getCode());
        if(entity!=null){
            throw CommandExceptionBuilder.exception(ErrorCodes.TRADEMARK_CODE_EXISTS);
        }
    }

    public void validateUpdate(TrademarkEntity entity, TrademarkInput input){
        if(!entity.getCode().equals(input.getCode())){
            TrademarkEntity getEntityCodeByInput = repository.findByCode(input.getCode());
            if(getEntityCodeByInput != null && !entity.getCode().equals(input.getCode())) {
                throw CommandExceptionBuilder.exception(ErrorCodes.TRADEMARK_CODE_EXISTS);
            }
        }
    }
}
