package com.afs.backend.commonservice.controller;

import com.afs.backend.base.common.service.ResponseService;
import com.afs.backend.base.controller.BaseController;
import com.afs.backend.commonservice.dto.attributegroupmanagement.AttributeGroupManagementInput;
import com.afs.backend.commonservice.dto.attributegroupmanagement.AttributeGroupManagementOutput;
import com.afs.backend.commonservice.entity.AttributeGroupManagementEntity;
import com.afs.backend.commonservice.service.attributegroupmanagement.AttributeGroupManagementService;
import com.afs.backend.commonservice.utils.ListPageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Map;

@RestController
@RequestMapping("/api/attribute_group_management")
public class AttributeGroupManagementController extends BaseController<AttributeGroupManagementService> {


    protected AttributeGroupManagementController(AttributeGroupManagementService service, ResponseService responseService) {
        super(service, responseService);
    }

    @PostMapping
    AttributeGroupManagementEntity create(@RequestBody AttributeGroupManagementInput input) {
        return getService().create(input);
    }

    @DeleteMapping("/{id}")
    void delete(@PathVariable("id") Long id){
        getService().delete(id);
    }

    @PutMapping("/{id}")
    AttributeGroupManagementEntity update(@PathVariable("id") Long id, @RequestBody AttributeGroupManagementInput input) {
        return getService().update(id, input);
    }

    @GetMapping
    ListPageResponse<AttributeGroupManagementOutput> getPage(@RequestHeader(value = "accept-language") String language, @ApiIgnore @RequestParam(required = false) Map<String, String> urlParams) {
        Page<AttributeGroupManagementEntity> entityPage = getService().getListAttributeGroupManagementEntity(language, urlParams);
        return getService().getListAttributeGroupManagementOutput(entityPage);
    }
}
