package com.afs.backend.commonservice.controller;

import com.afs.backend.base.common.response.Response;
import com.afs.backend.base.common.service.ResponseService;
import com.afs.backend.base.controller.BaseController;
import com.afs.backend.commonservice.dto.pageproductattribute.PageProductAttributeOutput;
import com.afs.backend.commonservice.dto.productmanagement.ProductManagementInput;
import com.afs.backend.commonservice.dto.productmanagement.ProductManagementOutput;
import com.afs.backend.commonservice.dto.productmanagementmapproperty.ProductManagementMapPropertyOutput;
import com.afs.backend.commonservice.dto.productmanagementmapvariationinput.ProductManagementMapVariationOutput;
import com.afs.backend.commonservice.dto.propertymanagement.PropertyManagementOutput;
import com.afs.backend.commonservice.entity.PageProductAttributeEntity;
import com.afs.backend.commonservice.entity.ProductManagementEntity;
import com.afs.backend.commonservice.entity.ProductManagementMapVariationEntity;
import com.afs.backend.commonservice.service.productmanagement.ProductManagementService;
import com.afs.backend.commonservice.utils.ListPageResponse;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/product_management")
public class ProductManagementController extends BaseController<ProductManagementService> {
    protected ProductManagementController(ProductManagementService service, ResponseService responseService) {
        super(service, responseService);
    }
    @PostMapping
    Response<ProductManagementEntity> create(@RequestParam("file_product")MultipartFile[] fileProduct, @RequestParam("file_variation") List<MultipartFile[]> fileVariation, @RequestPart("data")ProductManagementInput input) throws IOException {
        return success(getService().create(input, fileProduct, fileVariation));
    }

    @GetMapping
    ListPageResponse<ProductManagementOutput> getPage(@RequestHeader(value = "accept-language") String language, @ApiIgnore @RequestParam(required = false) Map<String, String> urlParams) {
        Page<ProductManagementEntity> entityPage = getService().getListProductManagementEntity(language, urlParams);
        return getService().getListProductManagementOutput(entityPage);
    }

    @GetMapping("/page-attribute")
    ListPageResponse<ProductManagementMapVariationOutput> getPage1(@RequestHeader(value = "accept-language") String language, @ApiIgnore @RequestParam(required = false) Map<String, String> urlParams) {
        Page<ProductManagementMapVariationEntity> entityPage = getService().getListProductManagementVariationEntity(language, urlParams);
        return getService().getListProductManagementVariationOutput(entityPage);
    }

    @GetMapping("/page-attribute/abc")
    ListPageResponse<PageProductAttributeOutput> getPage12(@RequestHeader(value = "accept-language") String language, @ApiIgnore @RequestParam(required = false) Map<String, String> urlParams) {
        Page<PageProductAttributeEntity> entityPage = getService().getListPageProductAttributeEntity(language, urlParams);
        return getService().getListPageProductAttributeOutput(entityPage);
    }

    @GetMapping("/detail-atttribute")
    ProductManagementMapPropertyOutput detailProperty(String code){
        return getService().detailProperty(code);
    }
}
