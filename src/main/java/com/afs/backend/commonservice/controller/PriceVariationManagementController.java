package com.afs.backend.commonservice.controller;

import com.afs.backend.base.common.service.ResponseService;
import com.afs.backend.base.controller.BaseController;
import com.afs.backend.commonservice.dto.pricevariationmanagement.PriceVariationManagementInput;
import com.afs.backend.commonservice.dto.pricevariationmanagement.PriceVariationManagementOutput;
import com.afs.backend.commonservice.service.pricevariationmanagement.PriceVariationManagementService;
import com.afs.backend.commonservice.entity.PriceVariationManagementEntity;
import com.afs.backend.commonservice.utils.ListPageResponse;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Map;

@RestController
@RequestMapping("/api/price_variation_management")
public class PriceVariationManagementController extends BaseController<PriceVariationManagementService> {

    protected PriceVariationManagementController(PriceVariationManagementService service, ResponseService responseService) {
        super(service, responseService);
    }

    @PostMapping
    PriceVariationManagementOutput create(@RequestBody PriceVariationManagementInput input){
        PriceVariationManagementEntity entity = getService().create(input);
        return getService().getOutputByEntity(entity);
    }

    @GetMapping("/{id}")
    PriceVariationManagementOutput detail(@PathVariable("id")Long id){
        PriceVariationManagementEntity entity = getService().detail(id);
        return getService().getOutputByEntity(entity);
    }

    @DeleteMapping("/{id}")
    void delete(@PathVariable("id")Long id){
        getService().deleteById(id);
    }

    @PutMapping("/{id}")
    PriceVariationManagementOutput update(@PathVariable("id")Long id, @RequestBody PriceVariationManagementInput input){
        PriceVariationManagementEntity entity = getService().update(id, input);
        return getService().getOutputByEntity(entity);
    }

    @GetMapping
    ListPageResponse<PriceVariationManagementOutput> getPage(@RequestHeader(value = "accept-language") String language, @ApiIgnore @RequestParam(required = false) Map<String, String> urlParams) {
        Page<PriceVariationManagementEntity> entityPage = getService().getListPriceVariationManagementEntity(language, urlParams);
        return getService().getListPriceVariationManagementOutput(entityPage);
    }
}
