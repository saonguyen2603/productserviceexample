package com.afs.backend.commonservice.controller;

import com.afs.backend.base.common.service.ResponseService;
import com.afs.backend.base.controller.BaseController;
import com.afs.backend.commonservice.dto.propertymanagement.PropertyManagementInput;
import com.afs.backend.commonservice.dto.propertymanagement.PropertyManagementOutput;
import com.afs.backend.commonservice.entity.PropertyManagementEntity;
import com.afs.backend.commonservice.service.propertymanagement.PropertyManagementService;
import com.afs.backend.commonservice.utils.ListPageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Map;

@RestController
@RequestMapping("/api/property_management")
public class PropertyManagementController extends BaseController<PropertyManagementService> {

    protected PropertyManagementController(PropertyManagementService service, ResponseService responseService) {
        super(service, responseService);
    }

    @PostMapping
    PropertyManagementOutput create(@RequestBody PropertyManagementInput input){
        PropertyManagementEntity entity = getService().create(input);
        return getService().getEntityToOutput(entity);
    }

    @DeleteMapping("/{id}")
    void deleteById(@PathVariable("id") Long id){
        getService().deleteById(id);
    }

    @PutMapping("/{id}")
    PropertyManagementOutput update(@PathVariable("id") Long id, @RequestBody PropertyManagementInput input){
        PropertyManagementEntity entity = getService().update(id, input);
        return getService().getEntityToOutput(entity);
    }

    @GetMapping("/{id}")
    PropertyManagementOutput detail(@PathVariable("id") Long id){
        PropertyManagementEntity entity = getService().detail(id);
        return getService().getEntityToOutput(entity);
    }

    @GetMapping
    ListPageResponse<PropertyManagementOutput> getPage(@RequestHeader(value = "accept-language") String language, @ApiIgnore @RequestParam(required = false) Map<String, String> urlParams) {
        Page<PropertyManagementEntity> entityPage = getService().getListPropertyManagementEntity(language, urlParams);
        return getService().getListPropertyManagementOutput(entityPage);
    }
}
