package com.afs.backend.commonservice.controller;

import com.afs.backend.base.common.service.ResponseService;
import com.afs.backend.base.controller.BaseController;
import com.afs.backend.commonservice.dto.trademark.TrademarkInput;
import com.afs.backend.commonservice.dto.trademark.TrademarkOutput;
import com.afs.backend.commonservice.entity.TrademarkEntity;
import com.afs.backend.commonservice.service.trademark.TrademarkService;
import com.afs.backend.commonservice.utils.ListPageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Map;

@RestController
@RequestMapping("/api/trademark")
public class TrademarkController extends BaseController<TrademarkService>{
    protected TrademarkController(TrademarkService service, ResponseService responseService) {
        super(service, responseService);
    }


    @PostMapping
    TrademarkEntity create(@Valid @RequestPart("data") TrademarkInput input, @ModelAttribute @RequestParam("file") MultipartFile file) throws IOException {
        return getService().create(input, file);
    }

    @PutMapping("/{id}")
    TrademarkEntity update(@PathVariable("id") Long id, @Valid @RequestPart("data") TrademarkInput input, @ModelAttribute @RequestParam("file") MultipartFile file) throws IOException {
        return getService().update(id, input, file);
    }

    @DeleteMapping("/{id}")
    void delete(@PathVariable("id") Long id) throws IOException {
        getService().deleteById(id);
    }

    @GetMapping
    ListPageResponse<TrademarkOutput> getPage(@RequestHeader(value = "accept-language") String language, @ApiIgnore @RequestParam(required = false) Map<String, String> urlParams) {
        Page<TrademarkEntity> entityPage = getService().getListTrademarkEntity(language, urlParams);
        return getService().getListTrademarkOutput(entityPage);
    }
}
