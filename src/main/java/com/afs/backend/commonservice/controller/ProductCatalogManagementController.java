package com.afs.backend.commonservice.controller;

import com.afs.backend.base.common.service.ResponseService;
import com.afs.backend.base.controller.BaseController;
import com.afs.backend.commonservice.dto.pricevariationmanagement.PriceVariationManagementOutput;
import com.afs.backend.commonservice.dto.productcatalogmanagement.ProductCatalogManagementInput;
import com.afs.backend.commonservice.dto.productcatalogmanagement.ProductCatalogManagementOutput;
import com.afs.backend.commonservice.dto.productcatalogmanagement.ProductCatalogManagementOutputForPriceVariation;
import com.afs.backend.commonservice.dto.propertymanagement.PropertyManagementOutput;
import com.afs.backend.commonservice.entity.ProductCatalogManagementEntity;
import com.afs.backend.commonservice.service.productcatalogmanagement.ProductCatalogManagementService;
import com.afs.backend.commonservice.dto.productcatalogmanagement.ProductCatalogManagementOutputForAttribute;
import com.afs.backend.commonservice.utils.ListPageResponse;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import java.io.IOException;
import java.util.Map;

@RestController
@RequestMapping("/api/product_catalog_management")
public class ProductCatalogManagementController extends BaseController<ProductCatalogManagementService> {
    protected ProductCatalogManagementController(ProductCatalogManagementService service, ResponseService responseService) {
        super(service, responseService);
    }


    @PostMapping
    ProductCatalogManagementEntity create(@RequestPart("data") ProductCatalogManagementInput input, @RequestParam MultipartFile file) throws IOException {
        return getService().create(input, file);
    }

    @GetMapping
    ListPageResponse<ProductCatalogManagementOutput> getPage(@RequestHeader(value = "accept-language") String language, @ApiIgnore @RequestParam(required = false) Map<String, String> urlParams) {
        Page<ProductCatalogManagementEntity> entityPage = getService().getListProductCatalogManagementEntity(language, urlParams);
        return getService().getListProductCatalogManagementOutput(entityPage);
    }

    @DeleteMapping("/{id}")
    void delete(@PathVariable("id") Long id) throws IOException {
        getService().delete(id);
    }

    @PutMapping("/{id}")
    ProductCatalogManagementEntity update(@PathVariable("id") Long id, @RequestPart("data") ProductCatalogManagementInput input, @RequestParam MultipartFile file) throws IOException {
        return getService().update(id, input, file);
    }

    @GetMapping("/attribute-group/{id}")
    ProductCatalogManagementOutputForAttribute detailAttributeGroup(@PathVariable("id") Long id) {
        return getService().getAttributeGroupList(id);
    }

    @GetMapping("/attribute-group/code")
    PropertyManagementOutput detailAttribute(@RequestParam String code) {
        return getService().detailAttribute(code);
    }

    @GetMapping("/price-variation/{id}")
    ProductCatalogManagementOutputForPriceVariation detailPriceVariationList(@PathVariable("id") Long id){
        return getService().getPriceVariationList(id);
    }

    @GetMapping("/price-variation/code")
    PriceVariationManagementOutput detailPriceVariation(@RequestParam String code){
        return getService().detailPriceVariation(code);
    }
}
