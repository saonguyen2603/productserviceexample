package com.afs.backend.commonservice;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.models.OpenAPI;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

@RequiredArgsConstructor
@SpringBootApplication(scanBasePackages = {"com.afs.backend.commonservice", "com.afs.backend.base"})
@EnableFeignClients
@OpenAPIDefinition(info = @Info(title = "EXAMPLE API", version = "1.0"),
        servers = {@Server(url = "/", description = "Default Server URL")})
public class Main {
    public static void main(String[] args) {

        SpringApplication.run(Main.class, args);
    }

    @Bean
    public OpenAPI openApiConfig() {
        return new OpenAPI().info(apiInfo());
    }

    public io.swagger.v3.oas.models.info.Info apiInfo() {
        io.swagger.v3.oas.models.info.Info info = new io.swagger.v3.oas.models.info.Info();
        info
                .title("Demo API")
                .description("Demo CRUD API")
                .version("v1.0.0");
        return info;
    }
}