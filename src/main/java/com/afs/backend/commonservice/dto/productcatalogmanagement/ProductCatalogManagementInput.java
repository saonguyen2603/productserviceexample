package com.afs.backend.commonservice.dto.productcatalogmanagement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.afs.backend.commonservice.dto.productcatalogmanagementmap.ProductCatalogManagementMapInput;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class ProductCatalogManagementInput {
    private String code;
    private String name;
    private String description;
    List<ProductCatalogManagementMapInput> mapInputList;
}
