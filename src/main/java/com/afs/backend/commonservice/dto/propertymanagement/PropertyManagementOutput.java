package com.afs.backend.commonservice.dto.propertymanagement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class PropertyManagementOutput {
    private String code;
    private String name;
    private String attributeType;
    private Object attributeGroupManagementId;
    private String sharingMode;
    private String description;
    private Boolean priceChange;
    private Boolean createExtraCode;
    private Object listMap;
}
