package com.afs.backend.commonservice.dto.propertymanagement;

import com.afs.backend.commonservice.dto.propertymanagemententitymap.PropertyManagementMapInput;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.validation.constraints.Pattern;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class PropertyManagementInput {
    private String code;
    private String name;
    @Pattern(regexp = "^(CHECK_BOX|TEXT_FIELD)", message = "{ERR034}")
    private String attributeType;
    private Long attributeGroupManagementId;
    @Pattern(regexp = "^(PUBLIC|PRIVATE)", message = "{ERR034}")
    private String sharingMode;
    private String description;
    private Boolean priceChange;
    private Boolean createExtraCode;
    private List<PropertyManagementMapInput> propertyManagementMapInputList;
}
