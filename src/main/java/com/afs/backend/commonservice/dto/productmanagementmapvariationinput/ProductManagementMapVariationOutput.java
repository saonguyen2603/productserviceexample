package com.afs.backend.commonservice.dto.productmanagementmapvariationinput;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class ProductManagementMapVariationOutput {
    private Object linkImageVariation;
    private String codeProduct;
    private Object productCatalog;
    private Object productManagement;
    private Object priceVariationManagement;


}
