package com.afs.backend.commonservice.dto.productmanagement;

import com.afs.backend.commonservice.enums.SharingMode;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class ProductManagementOutput {
    private Object linkImage;
    private String code;
    private String name;
    private Object productCatalogManagement;
    private String producer;
    private String unit;
    private SharingMode uses;
}
