package com.afs.backend.commonservice.dto.productmanagement;

import com.afs.backend.commonservice.dto.productmanagementmapproperty.ProductManagementMapPropertyInput;
import com.afs.backend.commonservice.dto.productmanagementmapvariationinput.ProductManagementMapVariationInput;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Pattern;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class ProductManagementInput {
    private String code;
    private String name;
    private Long productCatalogManagementId;
    private String producer;
    @Pattern(regexp = "^(ONE|SHIPMENT)", message = "{ERR034}")
    private String unit;
    private String description;
    @Pattern(regexp = "^(PUBLIC|PRIVATE)", message = "{ERR034}")
    private String uses;
    private Boolean batchCodeGenerationSupport;
    private Boolean informationProperties;
    private String informationPropertiesHowToEnter;
    private Boolean priceVariation;
    private String priceVariationHowToEnter;
    private Boolean isStatus;
    private List<ProductManagementMapPropertyInput> mapPropertyInputList;
    private List<ProductManagementMapVariationInput> mapVariationInputList;
}
