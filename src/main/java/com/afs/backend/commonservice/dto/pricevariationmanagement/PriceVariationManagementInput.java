package com.afs.backend.commonservice.dto.pricevariationmanagement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.afs.backend.commonservice.dto.pricevariationmanagementmap.PriceVariationManagementMapInput;

import javax.validation.constraints.Pattern;
import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class PriceVariationManagementInput {
    private String name;
    private String code;
    @Pattern(regexp = "^(NORMAL|COLOR)", message = "{ERR034}")
    private String colorType;
    private List<PriceVariationManagementMapInput> attribute;
}
