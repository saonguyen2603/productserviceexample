package com.afs.backend.commonservice.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Objects;

public enum Unit {
    ONE("ONE"), SHIPMENT("SHIPMENT");
    private final String s;

    Unit(String s) {
        this.s = s;
    }

    public static Unit valueOfUnit(String s) {
        if (Objects.nonNull(s)) {
            for (Unit e : values()) {
                if (e.s.equals(s)) {
                    return e;
                }
            }
        }
        return null;
    }

    @JsonValue
    public String getValue() {
        return this.s;
    }
}
