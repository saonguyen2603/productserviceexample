package com.afs.backend.commonservice.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Objects;

public enum SharingMode {
    PUBLIC("PUBLIC"), PRIVATE( "PRIVATE");

    private final String s;

    SharingMode(String s) {
        this.s = s;
    }

    public static SharingMode valueOfSharingMode(String s) {
        if (Objects.nonNull(s)) {
            for (SharingMode e : values()) {
                if (e.s.equals(s)) {
                    return e;
                }
            }
        }
        return null;
    }

    @JsonValue
    public String getValue() {
        return this.s;
    }
}
