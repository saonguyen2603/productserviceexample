package com.afs.backend.commonservice.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Objects;

public enum AttributeType {
    CHECK_BOX("CHECK_BOX"), TEXT_FIELD( "TEXT_FIELD");

    private final String s;

    AttributeType(String s) {
        this.s = s;
    }

    public static AttributeType valueOfAttributeType(String s) {
        if (Objects.nonNull(s)) {
            for (AttributeType e : values()) {
                if (e.s.equals(s)) {
                    return e;
                }
            }
        }
        return null;
    }

    @JsonValue
    public String getValue() {
        return this.s;
    }
}
