package com.afs.backend.commonservice.enums;

import javax.persistence.Converter;

@Converter
public class SharingModeConverter<S, S1> implements javax.persistence.AttributeConverter<SharingMode, String> {
    @Override
    public String convertToDatabaseColumn(SharingMode sharingMode) {
        return sharingMode != null ? sharingMode.getValue() : null;
    }

    @Override
    public SharingMode convertToEntityAttribute(String s) {
        return SharingMode.valueOfSharingMode(s);
    }
}
