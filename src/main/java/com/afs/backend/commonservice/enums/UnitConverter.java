package com.afs.backend.commonservice.enums;

import javax.persistence.Converter;

@Converter
public class UnitConverter<S, S1> implements javax.persistence.AttributeConverter<Unit, String> {
    @Override
    public String convertToDatabaseColumn(Unit unit) {
        return unit != null ? unit.getValue() : null;
    }

    @Override
    public Unit convertToEntityAttribute(String s) {
        return Unit.valueOfUnit(s);
    }
}
