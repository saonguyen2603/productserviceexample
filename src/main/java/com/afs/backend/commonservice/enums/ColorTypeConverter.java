package com.afs.backend.commonservice.enums;

import javax.persistence.Converter;

@Converter
public class ColorTypeConverter<S, S1> implements javax.persistence.AttributeConverter<ColorType, String> {
    @Override
    public String convertToDatabaseColumn(ColorType colorType) {
        return colorType != null ? colorType.getValue() : null;
    }

    @Override
    public ColorType convertToEntityAttribute(String s) {
        return ColorType.valueOfColorType(s);
    }
}