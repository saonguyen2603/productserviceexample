package com.afs.backend.commonservice.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Objects;

public enum ColorType {
    NORMAL("normal"), COLOR("màu sắc");

    private final String s;
    ColorType(String s) {
        this.s = s;
    }

    public static ColorType valueOfColorType(String s) {
        if (Objects.nonNull(s)) {
            for (ColorType e : values()) {
                if (e.s.equals(s)) {
                    return e;
                }
            }
        }
        return null;
    }

    @JsonValue
    public String getValue() {
        return this.s;
    }
}
