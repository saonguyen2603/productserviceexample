package com.afs.backend.commonservice.enums;

import javax.persistence.Converter;

@Converter
public class AttributeTypeConverter<S, S1> implements javax.persistence.AttributeConverter<AttributeType, String> {
    @Override
    public String convertToDatabaseColumn(AttributeType attributeType) {
          return attributeType != null ? attributeType.getValue() : null;
    }

    @Override
    public AttributeType convertToEntityAttribute(String s) {
        return AttributeType.valueOfAttributeType(s);
    }
}
