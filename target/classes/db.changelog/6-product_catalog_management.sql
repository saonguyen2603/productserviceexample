CREATE TABLE product_catalog_management
(
    id           bigserial primary key,
    code         varchar,
    name         varchar,
    picture_link varchar,
    public_id_picture varchar,
    description  varchar,
    created_at   DATE,
    updated_at   DATE,
    created_by   BIGINT,
    updated_by   BIGINT
);
CREATE TABLE product_catalog_management_map
(
    id                            bigserial primary key,
    product_catalog_management_id BIGINT references product_catalog_management (id),
    price_variation_management_id BIGINT references price_variation_management (id),
    attribute_group_management_id BIGINT references attribute_group_management (id)
);
