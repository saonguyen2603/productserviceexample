CREATE TABLE "property_management"
(
    "id"                            bigserial PRIMARY KEY,
    "code"                          varchar NOT NULL,
    "name"                          varchar NOT NULL,
    "attribute_type"                varchar,
    "attribute_group_management_id" bigint references attribute_group_management (id),
    "sharing_mode"                  varchar,
    "description"                   varchar,
    "price_change"                  boolean,
    "create_extra_code"             boolean,
    created_at                      DATE,
    updated_at                      DATE,
    created_by                      BIGINT,
    updated_by                      BIGINT
);

CREATE TABLE "property_management_map"
(
    "id"                     bigserial PRIMARY KEY,
    "property_management_id" bigint references property_management (id),
    "name"                   varchar,
    "alias"                  varchar,
    "order_number"           BIGINT,
    "is_default"             boolean,
    created_at               DATE,
    updated_at               DATE,
    created_by               BIGINT,
    updated_by               BIGINT
);