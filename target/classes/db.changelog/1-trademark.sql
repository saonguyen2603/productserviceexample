CREATE TABLE "trademark"
(
    "id"          bigserial PRIMARY KEY,
    "code"        varchar NOT NULL,
    "name"        varchar NOT NULL,
    "description" varchar,
    "logo"        varchar,
    "public_id_image" varchar,
    "is_active"   boolean
);