CREATE TABLE "attribute_group_management"
(
    "id"          bigserial PRIMARY KEY,
    "code"        varchar NOT NULL,
    "name"        varchar NOT NULL,
    "description" varchar,
    "is_active"   boolean,
    created_at    DATE,
    updated_at    DATE,
    created_by    BIGINT,
    updated_by    BIGINT
);