package com.afs.backend.commonservice.service.pricevariationmanagement;

import com.afs.backend.commonservice.dto.pricevariationmanagement.PriceVariationManagementInput;
import com.afs.backend.commonservice.dto.pricevariationmanagement.PriceVariationManagementOutput;
import com.afs.backend.commonservice.entity.PriceVariationManagementEntity;
import com.afs.backend.commonservice.enums.ColorType;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-04-04T13:48:07+0700",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 11.0.18 (Amazon.com Inc.)"
)
@Component
public class PriceVariationManagementMapperImpl implements PriceVariationManagementMapper {

    @Override
    public PriceVariationManagementEntity mapInputToEntity(PriceVariationManagementInput input) {
        if ( input == null ) {
            return null;
        }

        PriceVariationManagementEntity priceVariationManagementEntity = new PriceVariationManagementEntity();

        priceVariationManagementEntity.setName( input.getName() );
        priceVariationManagementEntity.setCode( input.getCode() );
        if ( input.getColorType() != null ) {
            priceVariationManagementEntity.setColorType( Enum.valueOf( ColorType.class, input.getColorType() ) );
        }

        return priceVariationManagementEntity;
    }

    @Override
    public PriceVariationManagementOutput mapEntityToOutput(PriceVariationManagementEntity entity) {
        if ( entity == null ) {
            return null;
        }

        PriceVariationManagementOutput priceVariationManagementOutput = new PriceVariationManagementOutput();

        priceVariationManagementOutput.setName( entity.getName() );
        priceVariationManagementOutput.setCode( entity.getCode() );
        if ( entity.getColorType() != null ) {
            priceVariationManagementOutput.setColorType( entity.getColorType().name() );
        }

        return priceVariationManagementOutput;
    }

    @Override
    public void update(PriceVariationManagementEntity entity, PriceVariationManagementInput input) {
        if ( input == null ) {
            return;
        }

        entity.setName( input.getName() );
        entity.setCode( input.getCode() );
        if ( input.getColorType() != null ) {
            entity.setColorType( Enum.valueOf( ColorType.class, input.getColorType() ) );
        }
        else {
            entity.setColorType( null );
        }
    }
}
