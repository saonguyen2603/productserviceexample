package com.afs.backend.commonservice.service.propertymanagement;

import com.afs.backend.commonservice.dto.propertymanagement.PropertyManagementInput;
import com.afs.backend.commonservice.dto.propertymanagement.PropertyManagementOutput;
import com.afs.backend.commonservice.entity.PropertyManagementEntity;
import com.afs.backend.commonservice.enums.AttributeType;
import com.afs.backend.commonservice.enums.SharingMode;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-04-04T13:48:07+0700",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 11.0.18 (Amazon.com Inc.)"
)
@Component
public class PropertyManagementMapperImpl implements PropertyManagementMapper {

    @Override
    public PropertyManagementEntity mapInputToEntity(PropertyManagementInput input) {
        if ( input == null ) {
            return null;
        }

        PropertyManagementEntity propertyManagementEntity = new PropertyManagementEntity();

        propertyManagementEntity.setCode( input.getCode() );
        propertyManagementEntity.setName( input.getName() );
        if ( input.getAttributeType() != null ) {
            propertyManagementEntity.setAttributeType( Enum.valueOf( AttributeType.class, input.getAttributeType() ) );
        }
        propertyManagementEntity.setAttributeGroupManagementId( input.getAttributeGroupManagementId() );
        if ( input.getSharingMode() != null ) {
            propertyManagementEntity.setSharingMode( Enum.valueOf( SharingMode.class, input.getSharingMode() ) );
        }
        propertyManagementEntity.setDescription( input.getDescription() );
        propertyManagementEntity.setPriceChange( input.getPriceChange() );
        propertyManagementEntity.setCreateExtraCode( input.getCreateExtraCode() );

        return propertyManagementEntity;
    }

    @Override
    public PropertyManagementOutput mapEntityToOutput(PropertyManagementEntity entity) {
        if ( entity == null ) {
            return null;
        }

        PropertyManagementOutput propertyManagementOutput = new PropertyManagementOutput();

        propertyManagementOutput.setCode( entity.getCode() );
        propertyManagementOutput.setName( entity.getName() );
        if ( entity.getAttributeType() != null ) {
            propertyManagementOutput.setAttributeType( entity.getAttributeType().name() );
        }
        propertyManagementOutput.setAttributeGroupManagementId( entity.getAttributeGroupManagementId() );
        if ( entity.getSharingMode() != null ) {
            propertyManagementOutput.setSharingMode( entity.getSharingMode().name() );
        }
        propertyManagementOutput.setDescription( entity.getDescription() );
        propertyManagementOutput.setPriceChange( entity.getPriceChange() );
        propertyManagementOutput.setCreateExtraCode( entity.getCreateExtraCode() );

        return propertyManagementOutput;
    }

    @Override
    public void update(PropertyManagementEntity entity, PropertyManagementInput input) {
        if ( input == null ) {
            return;
        }

        if ( input.getCode() != null ) {
            entity.setCode( input.getCode() );
        }
        if ( input.getName() != null ) {
            entity.setName( input.getName() );
        }
        if ( input.getAttributeType() != null ) {
            entity.setAttributeType( Enum.valueOf( AttributeType.class, input.getAttributeType() ) );
        }
        if ( input.getAttributeGroupManagementId() != null ) {
            entity.setAttributeGroupManagementId( input.getAttributeGroupManagementId() );
        }
        if ( input.getSharingMode() != null ) {
            entity.setSharingMode( Enum.valueOf( SharingMode.class, input.getSharingMode() ) );
        }
        if ( input.getDescription() != null ) {
            entity.setDescription( input.getDescription() );
        }
        if ( input.getPriceChange() != null ) {
            entity.setPriceChange( input.getPriceChange() );
        }
        if ( input.getCreateExtraCode() != null ) {
            entity.setCreateExtraCode( input.getCreateExtraCode() );
        }
    }
}
