package com.afs.backend.commonservice.service.productcatalogmanagementmap;

import com.afs.backend.commonservice.dto.productcatalogmanagementmap.ProductCatalogManagementMapInput;
import com.afs.backend.commonservice.entity.ProductCatalogManagementMapEntity;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-04-04T13:48:07+0700",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 11.0.18 (Amazon.com Inc.)"
)
@Component
public class ProductCatalogManagementMapMapperImpl implements ProductCatalogManagementMapMapper {

    @Override
    public ProductCatalogManagementMapEntity mapInputToEntity(ProductCatalogManagementMapInput input) {
        if ( input == null ) {
            return null;
        }

        ProductCatalogManagementMapEntity productCatalogManagementMapEntity = new ProductCatalogManagementMapEntity();

        productCatalogManagementMapEntity.setPriceVariationManagementId( input.getPriceVariationManagementId() );
        productCatalogManagementMapEntity.setAttributeGroupManagementId( input.getAttributeGroupManagementId() );

        return productCatalogManagementMapEntity;
    }
}
