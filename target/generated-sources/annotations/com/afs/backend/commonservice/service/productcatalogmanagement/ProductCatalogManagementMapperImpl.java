package com.afs.backend.commonservice.service.productcatalogmanagement;

import com.afs.backend.commonservice.dto.productcatalogmanagement.ProductCatalogManagementInput;
import com.afs.backend.commonservice.dto.productcatalogmanagement.ProductCatalogManagementOutput;
import com.afs.backend.commonservice.entity.ProductCatalogManagementEntity;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-04-04T13:48:06+0700",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 11.0.18 (Amazon.com Inc.)"
)
@Component
public class ProductCatalogManagementMapperImpl implements ProductCatalogManagementMapper {

    @Override
    public ProductCatalogManagementEntity mapInputToEntity(ProductCatalogManagementInput input) {
        if ( input == null ) {
            return null;
        }

        ProductCatalogManagementEntity productCatalogManagementEntity = new ProductCatalogManagementEntity();

        productCatalogManagementEntity.setCode( input.getCode() );
        productCatalogManagementEntity.setName( input.getName() );
        productCatalogManagementEntity.setDescription( input.getDescription() );

        return productCatalogManagementEntity;
    }

    @Override
    public ProductCatalogManagementOutput mapEntityToOutput(ProductCatalogManagementEntity entity) {
        if ( entity == null ) {
            return null;
        }

        ProductCatalogManagementOutput productCatalogManagementOutput = new ProductCatalogManagementOutput();

        productCatalogManagementOutput.setCode( entity.getCode() );
        productCatalogManagementOutput.setName( entity.getName() );
        productCatalogManagementOutput.setPictureLink( entity.getPictureLink() );
        productCatalogManagementOutput.setDescription( entity.getDescription() );

        return productCatalogManagementOutput;
    }

    @Override
    public void update(ProductCatalogManagementEntity entity, ProductCatalogManagementInput input) {
        if ( input == null ) {
            return;
        }

        if ( input.getCode() != null ) {
            entity.setCode( input.getCode() );
        }
        if ( input.getName() != null ) {
            entity.setName( input.getName() );
        }
        if ( input.getDescription() != null ) {
            entity.setDescription( input.getDescription() );
        }
    }
}
