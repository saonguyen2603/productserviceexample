package com.afs.backend.commonservice.service.propertymanagementmap;

import com.afs.backend.commonservice.dto.propertymanagemententitymap.PropertyManagementMapInput;
import com.afs.backend.commonservice.dto.propertymanagemententitymap.PropertyManagementMapOutput;
import com.afs.backend.commonservice.entity.PropertyManagementEntityMap;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-04-04T13:48:07+0700",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 11.0.18 (Amazon.com Inc.)"
)
@Component
public class PropertyManagementMapMapperImpl implements PropertyManagementMapMapper {

    @Override
    public PropertyManagementEntityMap mapInputToEntity(PropertyManagementMapInput input) {
        if ( input == null ) {
            return null;
        }

        PropertyManagementEntityMap propertyManagementEntityMap = new PropertyManagementEntityMap();

        propertyManagementEntityMap.setName( input.getName() );
        propertyManagementEntityMap.setAlias( input.getAlias() );
        propertyManagementEntityMap.setOrder( input.getOrder() );
        propertyManagementEntityMap.setDefault1( input.getDefault1() );

        return propertyManagementEntityMap;
    }

    @Override
    public PropertyManagementMapOutput mapEntityToOutput(PropertyManagementEntityMap entityMap) {
        if ( entityMap == null ) {
            return null;
        }

        PropertyManagementMapOutput propertyManagementMapOutput = new PropertyManagementMapOutput();

        propertyManagementMapOutput.setName( entityMap.getName() );
        propertyManagementMapOutput.setAlias( entityMap.getAlias() );
        propertyManagementMapOutput.setOrder( entityMap.getOrder() );
        propertyManagementMapOutput.setDefault1( entityMap.getDefault1() );

        return propertyManagementMapOutput;
    }
}
