package com.afs.backend.commonservice.service.trademark;

import com.afs.backend.commonservice.dto.trademark.TrademarkInput;
import com.afs.backend.commonservice.dto.trademark.TrademarkOutput;
import com.afs.backend.commonservice.entity.TrademarkEntity;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-04-04T13:48:07+0700",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 11.0.18 (Amazon.com Inc.)"
)
@Component
public class TrademarkMapperImpl implements TrademarkMapper {

    @Override
    public void update(TrademarkEntity entity, TrademarkInput input) {
        if ( input == null ) {
            return;
        }

        if ( input.getCode() != null ) {
            entity.setCode( input.getCode() );
        }
        if ( input.getName() != null ) {
            entity.setName( input.getName() );
        }
        if ( input.getDescription() != null ) {
            entity.setDescription( input.getDescription() );
        }
        if ( input.getIsActive() != null ) {
            entity.setIsActive( input.getIsActive() );
        }
    }

    @Override
    public TrademarkEntity mapInputToEntity(TrademarkInput input) {
        if ( input == null ) {
            return null;
        }

        TrademarkEntity trademarkEntity = new TrademarkEntity();

        trademarkEntity.setCode( input.getCode() );
        trademarkEntity.setName( input.getName() );
        trademarkEntity.setDescription( input.getDescription() );
        trademarkEntity.setIsActive( input.getIsActive() );

        return trademarkEntity;
    }

    @Override
    public TrademarkOutput mapEntityToOutput(TrademarkEntity entity) {
        if ( entity == null ) {
            return null;
        }

        TrademarkOutput trademarkOutput = new TrademarkOutput();

        trademarkOutput.setCode( entity.getCode() );
        trademarkOutput.setName( entity.getName() );
        trademarkOutput.setDescription( entity.getDescription() );
        trademarkOutput.setLogo( entity.getLogo() );
        trademarkOutput.setIsActive( entity.getIsActive() );

        return trademarkOutput;
    }
}
