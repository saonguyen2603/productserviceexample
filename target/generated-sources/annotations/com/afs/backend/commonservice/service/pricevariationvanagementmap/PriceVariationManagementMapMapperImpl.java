package com.afs.backend.commonservice.service.pricevariationvanagementmap;

import com.afs.backend.commonservice.dto.pricevariationmanagementmap.PriceVariationManagementMapInput;
import com.afs.backend.commonservice.dto.pricevariationmanagementmap.PriceVariationManagementMapOutput;
import com.afs.backend.commonservice.entity.PriceVariationManagementMapEntity;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-04-04T13:48:07+0700",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 11.0.18 (Amazon.com Inc.)"
)
@Component
public class PriceVariationManagementMapMapperImpl implements PriceVariationManagementMapMapper {

    @Override
    public PriceVariationManagementMapEntity mapInputToEntity(PriceVariationManagementMapInput input) {
        if ( input == null ) {
            return null;
        }

        PriceVariationManagementMapEntity priceVariationManagementMapEntity = new PriceVariationManagementMapEntity();

        priceVariationManagementMapEntity.setNameValue( input.getNameValue() );
        priceVariationManagementMapEntity.setOrder_number( input.getOrder_number() );
        priceVariationManagementMapEntity.setIsDefault( input.getIsDefault() );

        return priceVariationManagementMapEntity;
    }

    @Override
    public PriceVariationManagementMapOutput mapEntityToOutput(PriceVariationManagementMapEntity entity) {
        if ( entity == null ) {
            return null;
        }

        PriceVariationManagementMapOutput priceVariationManagementMapOutput = new PriceVariationManagementMapOutput();

        priceVariationManagementMapOutput.setNameValue( entity.getNameValue() );
        priceVariationManagementMapOutput.setOrder_number( entity.getOrder_number() );
        priceVariationManagementMapOutput.setIsDefault( entity.getIsDefault() );

        return priceVariationManagementMapOutput;
    }
}
