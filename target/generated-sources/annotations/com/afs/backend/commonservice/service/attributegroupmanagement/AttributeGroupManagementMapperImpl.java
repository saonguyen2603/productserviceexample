package com.afs.backend.commonservice.service.attributegroupmanagement;

import com.afs.backend.commonservice.dto.attributegroupmanagement.AttributeGroupManagementInput;
import com.afs.backend.commonservice.dto.attributegroupmanagement.AttributeGroupManagementOutput;
import com.afs.backend.commonservice.entity.AttributeGroupManagementEntity;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-04-04T13:48:07+0700",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 11.0.18 (Amazon.com Inc.)"
)
@Component
public class AttributeGroupManagementMapperImpl implements AttributeGroupManagementMapper {

    @Override
    public AttributeGroupManagementOutput mapEntityToOutput(AttributeGroupManagementEntity entity) {
        if ( entity == null ) {
            return null;
        }

        AttributeGroupManagementOutput attributeGroupManagementOutput = new AttributeGroupManagementOutput();

        attributeGroupManagementOutput.setCode( entity.getCode() );
        attributeGroupManagementOutput.setName( entity.getName() );
        attributeGroupManagementOutput.setDescription( entity.getDescription() );
        attributeGroupManagementOutput.setIsActive( entity.getIsActive() );

        return attributeGroupManagementOutput;
    }

    @Override
    public AttributeGroupManagementEntity mapInputToEntity(AttributeGroupManagementInput input) {
        if ( input == null ) {
            return null;
        }

        AttributeGroupManagementEntity attributeGroupManagementEntity = new AttributeGroupManagementEntity();

        attributeGroupManagementEntity.setCode( input.getCode() );
        attributeGroupManagementEntity.setName( input.getName() );
        attributeGroupManagementEntity.setDescription( input.getDescription() );
        attributeGroupManagementEntity.setIsActive( input.getIsActive() );

        return attributeGroupManagementEntity;
    }

    @Override
    public void update(AttributeGroupManagementEntity entity, AttributeGroupManagementInput input) {
        if ( input == null ) {
            return;
        }

        if ( input.getCode() != null ) {
            entity.setCode( input.getCode() );
        }
        if ( input.getName() != null ) {
            entity.setName( input.getName() );
        }
        if ( input.getDescription() != null ) {
            entity.setDescription( input.getDescription() );
        }
        if ( input.getIsActive() != null ) {
            entity.setIsActive( input.getIsActive() );
        }
    }
}
